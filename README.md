# Conflict Resolution Algorithms for Phylogenetic Networks #

Python implementation of dynamic programming + branch-and-bound solution for ODT (optimal display tree) under Deep Coalesence, Robinson-Foulds and Duplication costs with super-network inference.


References:

[1] Wawerka, M., Dąbkowski, D., Rutecka, N. et al. Embedding gene trees into phylogenetic networks by conflict resolution algorithms. Algorithms Mol Biol 17, 11 (2022). https://doi.org/10.1186/s13015-022-00218-8

[2] Rutecka, N., Mykowiecka, A., Paszek J., Górecki P. Robinson-Foulds distance between phylogenetic networks and gene trees, submitted

[3] Jerzy Tiuryn, Natalia Rutecka & Paweł Górecki, Rooting Gene Trees via Phylogenetic Networks, COCOON 2022, https://doi.org/10.1007/978-3-031-22105-7_37

## Usage ##

Main script is embnet.py. To set a specific cost function use option `-c COST`, where COST is one of the following:
- `C` - DC, deep coalescence non-classic (default, see [1]) 
- `c` - DC, classic DC costs
- `R` - RF, Robinson-Foulds distance
- `D` - D, duplication cost 
- `U` - DL, duplication loss-cost

### Computing ODT cost with DP and branch-and-bound 

Available costs: C, D and R.

DC non-classic (-cC)
```
> embnet.py -n "((a, (b)#H ), (#H, c) )" -g  "((a,b),(b,c))" -pb
6
```

As above for Robinson-Foulds
```
> embnet.py -g  "((a,b),c)"  -n "((a, (b)#H ), (#H, c) )" -pb -cR
0
```

As above for duplication 
```
> embnet.py -g  "((a,b),c)"  -n "((a, (b)#H ), (#H, c) )" -pb -cD
0
```


### Naive cost computation by comparing a given gene tree with all displayed trees

Cost under non-classic DC (-cC)
```
> embnet.py -n "((a, (b)#H ), (#H, c) )" -g  "((a,b),(b,c))" -pa
Network: (((b)#H,a),(#H,c))
((a,b),(b,c)) (a,(b,c)) 6 C
((a,b),(b,c)) ((b,a),c) 6 C
```

As above for DC-classic
```
> embnet.py -g  "((a,b),c)"  -n "((a, (b)#H ), (#H, c) )" -pa -cc
Network: (((b)#H,a),(#H,c))
((a,b),c) (a,(b,c)) 1 c
((a,b),c) ((b,a),c) 0 c
```

As above for Robinson-Foulds distance
```
> embnet.py -g  "((a,b),c)"  -n "((a, (b)#H ), (#H, c) )" -pa -cR
Network: (((b)#H,a),(#H,c))
((a,b),c) (a,(b,c)) 2 R
((a,b),c) ((b,a),c) 0 R
```


### Approximation of ODT cost with DP (lower bound computation):

DC non-classic (-cC)
```
> embnet.py -n "((a, (b)#H ), (#H, c) )" -g  "((a,b),(b,c))" -pe
6
```

For RF, by default we use fast DP computation (i.e., O(n^2) algorithm instead of O(n^3), see article [2] for more details)
```
> embnet.py -n "((a, (b)#H ), (#H, c) )" -g  "(a,(b,c))" -pe -cR
0
```

RF with slow DP
```
> embnet.py -n "((a, (b)#H ), (#H, c) )" -g  "(a,(b,c))" -pFe -cR
0
```


### Super-networks 

Super-networks with up to 3 (-k3) reticulations under RF; heuristic starts with 1 initial network (-K1) based on input gene trees 
```
embnet.py -S -g "((a,b),(c,d)); (a,(b,(c,d)));((c,a),(d,b))" -k3 -K1 -cR
```

Super-networks where two inital networks are provided; duplication cost 
```
> embnet.py -S -g"((a,b),c); ((a,a),(b,c))" -n"((a, (b)#H ), (#H, c) ); (a,(b,c))" -cD
  0%|          | 0/2 [00:00<?, ?it/s]100%|██████████| 2/2 [00:00<00:00, 378.74it/s]
Best cost: 1
Best net: (((b)#H,a),(#H,c))
```

## For more options, run:

`embnet.py -h`


### Dependencies ### 

Python v3.10+ is required.

Install dependencies via requirements.txt.

### Usage ###

```
embnet.py
```

## Basic input and output 

 `-g FILE/TREE(S)` - input gene tree(s); multiple trees can be defined in a ;-separated string;
 `-s FILE/TREE(S)` - input species tree(s)
 `-n FILE/TREE(S)` - reticulation network(s) in rich newick format:   
  `((a, ((b, (c, (d)Y#H1)), (((Y#H1, e)h, f))X#H2)), ((X#H2, g)d, 8)b)r`;
Cardona et al., 2008 BMC Bioinf.; comments are allowed in [...]


### Drawing trees and networks via dot package

 `-d filename` - gen dot file(s); see also `-pi`
 
### Print and evaluate 

 `-p STR` - print & evaluate:

     - s - print species tree
  	 - g - print gene tree  
     - n - print network
     - u - print networks using decomposion and sorted represenation (only for tree-child networks)
     - d - print all trees displayed by the network (see also -i)
     - l - print all trees displayed by the network with branch lenghts (see also -i)
     - e - DP for gene tree-network embedding (only C, R, D costs); see -pF
     - F - do not use fast DP with RF 
     - D - info on nodes of each network (debug)
     - c - compare gene and species tree 
     - a - compare gene and all trees displayed by the network
     - b - computing ODT cost by branch-and-bound algorithm; may call DP multiple times 
          only with C, R, D; see also -pF for fast DP with RF     
     - A - print Phi supports for each network; see also suboption r
     - Ar - as above but supports are computed only for the root of the input networks (gene trees ignored if present)
     - t - for every network print True if the network is tree-child otherwise print False
     - i - skip node ids in labels of nodes in dot generator (-d)

 `-i NUM` - print display tree no NUM (NUM in 0..2^reticulations-1)


### Edit operation on networks 

 - `-C "label1,label2,..."` - remove leaves having given labels from all networks
 - `-P "label1,label2,..."` - preserve leaves having given labels in all networks
 - `-R` - remove redundant reticulations (both parents of reticulation are the same or grandparent is the second parent)

### Generate quasi consensus trees from gene trees

 -q NUM - gen NUM quasi-consensus trees from gene trees


### RF-net distances between networks

 -D - compute RF-net distance between each pair of networks

### Super-network inference

 - `-S` - run super-network heuristic; 
 - `-k NUM` - maximal number of reticulations in a super-network; default is 5
 - `-K NUM` - number of initial networks if no initial network is provided

 - `-x` - if specified, use heuristics in branch-and-bound
 - `--limit INT` - how many branchings can we use before halting in branch-and-bound, this might result in incorrect result

### Examples 

Compare gene tree vs. species tree (DUP cost)
```
> embnet.py  -g "(((c,b),a),(e,d))" -s"(((b,a),c),(e,d))" -cD -pc
(((c,b),a),(e,d)) (((b,a),c),(e,d)) 1 DUP
```

Compare gene tree vs. species tree (DC cost non-classic)
```
> embnet.py  -g "(((c,b),a),(e,d))" -s"(((b,a),c),(e,d))" -pc
(((c,b),a),(e,d)) (((b,a),c),(e,d)) 9 DC (non-classic)
```

Print trees displayed by the network:
```
> embnet.py -n "((a, ((b)#Y)#H ), (#H, (c,#Y)))" -pd
Network: ((((b)#Y)#H,a),(#H,(#Y,c)))
(a,(b,c))
(a,(b,c))
(a,(b,c))
((b,a),c)
```

Embedding gene trees into networks (DP with DC non-classic cost only):
```
> embnet.py -n "((a, (b)#H ), (#H, c) )" -g  "((a,b),(b,c))" -p e
6
```

```
> embnet.py -n "((a, (b)#H ), (#H, c) )" -g  "(b,c)" -p e
2
```

```
> embnet.py -n "((((f, e), b))#H1, (#H1, (((d, c))#H2, (#H2, a))))" -g "((c, ((e, a), b)), d)" -p e
12
```



Embedding gene trees into networks with branch-and-bound (using DP with DC non-classic cost only):
```
> embnet.py -n "((a, (b)#H ), (#H, c) )" -g  "((a,b),(b,c))" -p b
6
```

```
> embnet.py -n "((a, (b)#H ), (#H, c) )" -g  "(b,c)" -p b
2
```

```
> embnet.py -n "((((f, e), b))#H1, (#H1, (((d, c))#H2, (#H2, a))))" -g "((c, ((e, a), b)), d)" -p b
13
```


Generate and view dot file (-d)
```
> embnet.py -n "((a, ((b)#Y)#H ), (#H, (c,#Y))) [Non tree-child N1]" -g "(a, b)"  -pe -d n1
2
> dot -Tpdf n1.dot -o n1.pdf # graphviz package required
```


Compute by DP, generate and view dot (file with delta annotations for nodes 0, 1 and 2 from the gene tree.
```
> embnet.py -n "((a, ((b)#Y)#H ), (#H, (c,#Y))) [Non tree-child N1]" -g "(a, b)"  -pe -d "n1delta:0:1:2"
2
> dot -Tpdf n1delta.dot -o n1delta.pdf
```


Naive cost computation by comparing a given gene tree with all displayed trees (DUP cost)
```
> embnet.py -g  "((a,b),c)"  -n "((a, (b)#H ), (#H, c) )" -pa -cD
Network: (((b)#H,a),(#H,c))
((a,b),c) (a,(b,c)) 1 D
((a,b),c) ((b,a),c) 0 D
```


Super-network inference using some predefined set of initial networks
```
> embnet.py -S -g"((a,b),c); ((a,a),(b,c))" -n"((a, (b)#H ), (#H, c) ); (a,(b,c))"
  0%|          | 0/2 [00:00<?, ?it/s]100%|██████████| 2/2 [00:00<00:00, 133.42it/s]
Best cost: -2
Best net: (((b)#H,a),(#H,c))
```


Naive DC-cost computation by comparing a given gene tree with all displayed trees 
```
> embnet.py -g  "((a,b),(b,c))"  -n "((a, (b)#H ), (#H, c) )" -pa
Network: (((b)#H,a),(#H,c))
((a,b),(b,c)) (a,(b,c)) 6 C
((a,b),(b,c)) ((b,a),c) 6 C
```


To generate random tree-child networks/trees in -n, -g or -s use rand:NUM1:NUM2 where NUM1 is the number of leaves 
and NUM2 is the number of reticulation nodes. Leaf labels are a,b,c,...,z,a1,b1,... etc, and reticulation labels are A,B,...,Z,A1,A2,...
```
> embnet.py -n rand:5:3 -g rand:5:0
```

(((#C,(a)#A),(((d,b))#B)#C),(c,(#A,(#B,e))))
((a,e),((b,c),d))

Dot-drawing random embeddings:
```
> embnet.py -nrand:10:5 -grand:10:0 -pe -dn && dot -Tpdf n.dot -o n.pdf 
23
```


Dot-drawing random network:
```
> embnet.py -nrand:10:5 -dn && dot -Tpdf n.dot -o n.pdf 
```

```
> embnet.py -nrand:100:20 -dn && dot -Tpdf n.dot -o n.pdf && evince n.pdf
```

 
RF-net:
```
embnet.py -n "(#A,(d,((a,c),(b,(e)#A)))); (#A,(d,((a,c),(b,(e)#A))))" -D
embnet.py -n "((c,b),(#A,((d,a),(e)#A))); (#A,(d,((a,c),(b,(e)#A)))); (#A,(d,((a,c),(b,(e)#A))))" -cD
```


Displayed trees with branch lengths:
```
> embnet.py -n"((c:2)#A:4,(#A:1,(b:2,a:5):1):2):2" -pnd
((c)#A,(#A,(b,a)))
Network: ((c)#A,(#A,(b,a)))
(c:3.0,(b:2.0,a:5.0):1.0):4.0
(c:6.0,(b:2.0,a:5.0):3.0):2.0
```

```
> embnet.py -n"((c:2)#A:4,(#A:1,(b,a:5)):2):2" -pnd
((c)#A,(#A,(b,a)))
Network: ((c)#A,(#A,(b,a)))
(c:3.0,(b,a:5.0)):4.0
(c:6.0,(b,a:5.0)):2.0
```

Contract:
```
> embnet.py -n"((((b)#A,c))#B,(#B,(#A,(a,d))))" -C "a,d" -pn
((((b)#A,c))#B,(#A,#B))
```

Remove redundant reticulations:
```
> embnet.py -n '((((b)#A,((c)#C,#C)))#B,(#A,#B))' -R -pn
(b,c)
```

Print sorted representation (unique for TC-networks)
```
> embnet.py -n '((((b)#A,((c)#C,#C)))#B,(#A,#B))' -R -pn
(b,c)
```

Computing Phi supports (used in gene tree rooting via networks)
```
> embnet.py -n '((a,b),(c,d))' -g "(c,d)" -pA 
(c,d) c;d 0
```

```
> embnet.py -n '((a,b),(c,d))' -g "(c,d)" -pAr 
((a,b),(c,d)) a,b;c,d 0
```

```
> embnet.py -n '((((b)#A,((c)#C,#C)))#B,(#A,#B))' -pA 
((((b)#A,((c)#C,#C)))#B,(#B,#A)) ; 0
```


Test whether the network is tree-child.
```
> embnet.py -n '((((b)#A,((c)#C,#C)))#B,(#A,#B))' -pt
False
```





### Questions ###

For questions, please contact:

<gorecki@mimuw.edu.pl>
