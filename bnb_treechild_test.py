import argparse
from pathlib import Path
from random import randint

from treeop import compcostsmp, str2tree, randtreestr, Tree, ftread
from netop import randdagstr, Network


def main():
    parser = argparse.ArgumentParser(description="Run random correctness tests for branch-and-bound")
    parser.add_argument("--dir", "-d", help="Directory to which bad tests will be written")
    parser.add_argument("--min_leaf", help="Minimum number of leaves for network", type=int, default=7)
    parser.add_argument("--max_leaf", help="Maximum number of leaves for network", type=int, default=15)
    parser.add_argument("--min_retic", help="Minimum number of reticulations for network", type=int, default=1)
    parser.add_argument("--max_retic", help="Maximum number of reticulations for network", type=int, default=4)
    parser.add_argument("--cost", help="Cost function used (C - DC cost, D - duplication cost, R- RF cost)",
                        default='R', choices=['D', 'C', 'R'])
    args = parser.parse_args()
    directory = "bad_tests"
    if args.min_leaf <= 1:
        print("Min_leaf has to be at least 2")
        exit(1)
    if args.min_retic < 0:
        print("Min_retic has to be at least 0")
        exit(1)
    if args.min_leaf > args.max_leaf:
        print("Min_leaf is bigger than max_leaf")
        exit(1)
    if args.min_retic > args.min_leaf - 2:
        print("Min_retic is bigger than min_leaf - 2")
        exit(1)
    if args.min_retic > args.max_retic:
        print("Min_retic is bigger than max_retic")
        exit(1)
    if args.dir:
        directory = args.dir
    tests_passed = 0
    bad_tests = 0
    Path(directory).mkdir(parents=True, exist_ok=True)
    while True:
        leaves = randint(args.min_leaf, args.max_leaf)
        reticulations = randint(args.min_retic, min(args.max_retic, leaves - 2))
        net = ftread(randdagstr(leaves, reticulations, networktype=0), Network)[0]
        tree_size = randint(2, leaves) if args.cost != "R" else leaves
        g = Tree(str2tree(randtreestr(tree_size, use_numbers=False)))
        min_cost = 10000000000
        for t in net.displayedtrees():
            min_cost = min(min_cost, compcostsmp(g, Tree(str2tree(t)), args.cost))
        cost = net.branch_and_bound(g, cost_typefunc=args.cost, fast_DP=False)
        if args.cost == "R":
            _, lower_bound, usage = net.ret_min_rf(g)
            _, lower_bound_true, usage_true = net.ret_min_rf_fast(g)
            is_passed = lower_bound == lower_bound_true and cost == min_cost
        else:
            is_passed = cost == min_cost

        if not is_passed:
            with open(Path(directory, f"test{bad_tests}"), 'w') as f:
                f.write(f"{net.root.netrepr()}\n{g}\n")
                f.write(f"Correct: {min_cost}\n")
                f.write(f"Incorrect: {cost}\n")
                f.write(f"Lower bounds {lower_bound}, expected: {lower_bound_true}\n")
                f.write(f"Usage {usage}, expected {usage_true}")
            bad_tests += 1
            if bad_tests == 100:
                print("100 bad tests reached, ending.")
                exit(0)
            print("Wrong test nr", bad_tests)
        else:
            tests_passed += 1
            print(tests_passed)


if __name__ == "__main__":
    main()
