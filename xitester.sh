



#rm -rf xit*.dot xit*.pdf

if [[ -f $1 ]]
then
	F=$1
	rm -rf /tmp/_xit_*.pdf
	nl $1 | while read a; 
	do

		set $( echo $a | tr ";" " " )
		NR=$1
		shift
		while [[ $# -gt 2 ]];
			do 
				shift 1
		done
		echo $NR $G $N
		G=$1
		N=$2


		rm -rf xit*.dot /tmp/xit*.pdf

		embnet.py -g $G -n $N -pS 
		STATUS=$?

		#for i in xit*.dot; do dot -Tpdf $i -o tmp/$( basename $i .dot).pdf; done
		dot -Tpdf xit_fin.dot -o /tmp/_xit_$NR.pdf

	done
	pdfunite /tmp/_xit_*.pdf $F.pdf
	echo $F.pdf created
	exit
fi

if [[ $1 ]]
then
	outputfile=$1
	Single=1

	if [[ $1 = p0 ]]
	then 		
		N='((a,((b,#1),#2)),(((c)#1,d))#2)'
		G='(d,(b,(c,a)))' 
	fi

	# wszystkie koszt = 1 
	if [[ $1 = p1 ]]
	then
		G='(a,((b,e),(c,d)))'
		N='((((d,((b)#A,a)))#B,e),(#B,(#A,c)))'		
	fi

	if [[ $1 = p2 ]]
	then
		G="((d,((a,e),b)),(c,f))"
		N="((c)#B,((((b,((e)#A,f)))#C,a),(#C,(#B,(#A,d)))))"
	fi

	if [[ $1 = p3 ]]
	then
		G="(e,((a,(d,b)),c))"
		N="((((((b)#D,(((#D,e))#C,d)))#B,(#B,c)))#A,(#A,(#C,a)))"
	fi
	
	if [[ $1 = p4 ]]
	then
	G="((a,(b,(c,e))),d)"
	N="((((c)#B,(#B,e)))#D,((((d)#C,a))#A,(#D,(#C,(#A,b)))))"
fi

	if [[ $1 = p5 ]]
	then
			G="((c,(d,a)),b)"
			N="((c)#C,(((#C,b))#B,(((#B,d))#A,(#A,a))))"
	fi

	if [[ $1 = p6 ]]
	then
		G="((f,(e,(c,(d,a)))),b)"
		N="(((b,((a)#A,((d)#C,e))))#B,((#A,c),(#C,(#B,f))))"
	fi

	if [[ $1 = ssx ]]
	then
	
	cat << EOF >> ssx
51;2;2700;540;((a,((c,d),(h,(e,g)))),(f,b));((g)#A,(((((#A,c))#B,((e)#D,((((#B,(#D,d)),f))#E,a))))#C,((#C,h),(#E,b))))	
51;1;6400;6400;(((((e,d),a),c),b),f);((((e)#C,((((((d)#E,(#C,c)))#B,(#B,f)))#D,(#E,(#D,b)))))#A,(#A,a))
51;1;2016;2016;(c,(b,(((f,a),e),(d,g))));(((f)#D,e),(((((((#D,((a)#E,g)))#B,(#B,c)))#C,(#E,b)))#A,(#A,(#C,d))))
51;2;108;36;(c,((f,((a,d),b)),(e,g)));(b,((g)#A,(((c,((a)#B,((d)#D,(#A,f)))))#C,(#B,(#D,(#C,e))))))
51;1;432;432;(a,(c,(h,(e,(d,((b,f),g))))));(g,((f)#A,(((((e)#D,(#D,a)),(((d,b))#B,(#A,h))))#C,(#C,(#B,c)))))
EOF
	xitester.sh ssx
	rm -f ssx 
	exit 0
fi

fi

if [[ $Single ]]
then	
	rm -rf xit*.dot xit*.pdf
	embnet.py -g $G -n $N -pS
	for i in xit*.dot; do dot -Tpdf $i -o $( basename $i .dot).pdf; done
	pdfunite xit*.pdf $outputfile.pdf
	echo $outputfile.pdf created
	exit 0
fi

DIR=xitest_$$ 

mkdir -p $DIR

PATH=$PATH:$PWD

cd $DIR

CNT=0
while true;
do
	RT=$(( 3 + RANDOM % 3 ))
	SP=$(( 5 + RANDOM % 4 ))

	G=$( embnet.py -n rand:$SP:0 -pn )
	N=$( embnet.py -n rand:$SP:$RT -pn )

	embnet.py -g $G -n $N -pS 
	STATUS=$?

	# search for DP!=naive and no superscenarios
	[[ $? -gt 3 ]] && break	
	[[ $? -lt 0 ||  $? -gt 15 ]] && break	

	(( CNT+=1 ))
	echo $CNT > cnt.log
done
	
for i in xit*.dot; do dot -Tpdf $i -o $( basename $i .dot).pdf; done
pdfunite xit*.pdf xit.pdf
