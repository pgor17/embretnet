import argparse
import glob
import multiprocessing as mp

from random import seed

from treeop import Tree, str2tree
from netop import Network, addretstr
from netsearch import get_init_nets, supernet_search, naive_rf
    

def printl(obj, dest): 
    print(obj, flush=True)
    print(obj, flush=True, file=dest)

def main():
    
    example_txt = """example:   
    python3 test_hc_corona.py --rcons 1 --rspec 1 --kmin 0 --kmax 1
    """
    
    desc = """Run netsearch hill climbing heuristic on coronavirus gene trees data."""
    
    parser = argparse.ArgumentParser(description=desc, epilog=example_txt,
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-d", help="Path to folder with gene trees data", type=str, default="coronavirus_data")
    parser.add_argument("-f", help="Target filename to save .txt report", type=str, default="hc_corona_results.txt")
    parser.add_argument("--gtrees", help="Which gene trees suite to use: 5, 8 or 10", type=int, default=5)
    parser.add_argument("--rcons", help="Number of quasi-cons initial nets used in each run", type=int, default=20)
    parser.add_argument("--rspec", help="Number of species tree initial nets used in each run", type=int, default=0)
    parser.add_argument("--kmin", help="Minimum number of reticulations", type=int, default=0)
    parser.add_argument("--kmax", help="Maximum number of reticulations", type=int, default=7)
    parser.add_argument("--relaxed", help="Whether to relax condition 3b", type=int, default=1)
    parser.add_argument("--time_consistent", help="Whether networks suitable for HGT model", type=int, default=1)
    parser.add_argument('--fastloop', action='store_true')
    parser.add_argument('--no-fastloop', dest='fastloop', action='store_false')
    parser.set_defaults(fastloop=True)
    parser.add_argument("--print_top", help="How many best networks to print", type=int, default=10)
    parser.add_argument("--seed", help="Seed for all random operations", type=int, default=1)
    args = parser.parse_args()
    
    gene_trees = []
#     for f in glob.glob(f"./{args.d}/_*_rooted.nwk"):
    if args.gtrees == 5:
        chosen_genes = ['M', 'ORF1ab', 'ORF3a', 'ORF6', 'S']
    elif args.gtrees == 8:
        chosen_genes = ['E', 'M', 'ORF1ab', 'ORF3a', 'ORF6', 'ORF8', 'ORF10', 'S']
    elif args.gtrees == 10:
        chosen_genes = ['E', 'M', 'ORF1ab', 'ORF3a', 'ORF6', 'ORF7a', 'ORF7b', 'ORF8', 'ORF10', 'S']
    else:
        raise ValueError(f"Got gtrees={args.gtrees}, expected 5, 8 or 10")
        
    for f in [f"./{args.d}/_{gene}_rooted.nwk" for gene in chosen_genes]:
        with open(f, "r") as tree_file:
            tree_str = tree_file.read()
            gene_trees.append(Tree(str2tree(tree_str)))
    
    true_nets_7ret = []
    for f in glob.glob(f"./{args.d}/7reticulations/*"):
        with open(f, "r") as true_net_file:
            true_net_str = true_net_file.read()
            true_nets_7ret.append(Network(str2tree(true_net_str)))
            
    true_nets_5ret = []
    for f in glob.glob(f"./{args.d}/5reticulations/*"):
        with open(f, "r") as true_net_file:
            true_net_str = true_net_file.read()
            true_nets_5ret.append(Network(str2tree(true_net_str)))
    
    with open(f"./{args.d}/_speciestree", "r") as species_tree_file:
        species_tree_str = species_tree_file.read()
            
    with open(args.f, "w") as f:
        printl("#"*40, f)
        printl(f"Initializing supernet search with seed={args.seed}", f)
        printl(f"Net class: relaxed={args.relaxed}, time_consistent={args.time_consistent}", f)
        printl(f"Choosing from rcons={args.rcons} initial networks", f)
        printl(f"and from rspec={args.rspec} init nets in parallel", f)
        printl("#"*40, f)
        printl('', f)
        
        tree_sizes = [len([x for x in tree.leaves()]) for tree in gene_trees]
        printl(f"### GENE TREES [{len(gene_trees)} collected] ###", f)
        for i, gene_tree in enumerate(gene_trees):
            printl(f"{chosen_genes[i]}: {tree_sizes[i]} species", f)
        printl('', f)

        species = {leaf.label for tree in gene_trees for leaf in tree.leaves()}
        printl(f"### ALL SPECIES [{len(species)} collected] ###", f)
        for sp in species:
            printl(sp, f)
        printl('', f)
        
        printl(f"### TRUE NETS COSTS [{len(true_nets_7ret)} collected] ###", f)
        printl(f"7 reticulations: {[sum(naive_rf(tree, net) for tree in gene_trees) for net in true_nets_7ret]}", f)
        printl(f"5 reticulations: {[sum(naive_rf(tree, net) for tree in gene_trees) for net in true_nets_5ret]}", f)
        printl('', f)

        init_nets_cons = get_init_nets(
            gene_trees, args.kmin, args.rcons, init_method='quasi-cons',
            relaxed=args.relaxed, time_consistent=args.time_consistent)
        init_nets_spec = get_init_nets(
            species_tree_str, args.kmin, args.rspec, init_method='species_tree',
            relaxed=args.relaxed, time_consistent=args.time_consistent)
        init_nets = init_nets_cons + init_nets_spec
        max_threads = min(len(init_nets), mp.cpu_count())
        
        for k in range(args.kmin, args.kmax + 1):
            seed(args.seed)
            printl(f"### SUPERNET SEARCH k={k} ###", f)
            init_nets_copy = [Network(str2tree(net.root.netrepr())) for net in init_nets]
            output = supernet_search(gene_trees, init_nets, random_state=args.seed, threads=max_threads,
                                     relaxed=args.relaxed, time_consistent=args.time_consistent, fast_loop=args.fastloop)
            
            printl(f"### TOTAL_TIME: {output['total_time']:.0f} sec ###", f)
            init_types = ['cons'] * args.rcons + ['spec'] * args.rspec
            pairs = [(x, y, z) for x, y, z in zip(output['cost'], output['net'], init_types)]
            pairs.sort(key=lambda x: x[0])
            for i, (dc, net, init_type) in enumerate(pairs[:args.print_top]):
                printl(f"{i+1}. {init_type} [DC={dc}] {net}", f)
            if len(pairs) > args.print_top:
                dc, net, init_type = pairs[-1]
                printl(f"...", f)
                printl(f"{len(pairs)}. {init_type} [DC={dc}]", f)
            printl('', f)
            init_nets = [Network(str2tree(addretstr(net.root.netrepr(), 1, skip=k, networktype=int(args.relaxed), 
                  time_consistent=args.time_consistent))) for dc, net, init_type in pairs]
    
    
if __name__ == "__main__":
    main()
