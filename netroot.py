
import itertools
from treeop import Tree, str2tree
from itertools import chain, combinations, product
from netop   import Network

def get_tree_leaves(v):        
        if v.reticulation: return frozenset()
        if not v.c: return frozenset([v])        
        return frozenset.union(*(get_tree_leaves(c) for c in v.c))

def get_tree_leaflabels(v):                
        return [ w.label for w in get_tree_leaves(v) ]


def smalldeltafun(x,y,a,b):
    """
    d(x, y) is a predicate that is true if and only if there are node-disjoint paths from x to a and from y to b 
    """

    def droot(x,y):                    
        if x==y: return False
        if x==a and y==b: return True
        if x.reticulation: return droot(x.c[0],y)
        elif len(x.c)==2:
            return droot(x.c[0],y) or droot(x.c[1],y)
        if y.reticulation: return droot(x,y.c[0])
        elif len(y.c)==2:
            return droot(x,y.c[0]) or droot(x,y.c[1])
        return False

    return droot(x,y) or droot(y,x)

def phiSupportsRootLevel(n):
    """
    Generates classes of delta for the left and right child of the root of N
    """
    leaves = n.get_leaves() 
    cabs = []
            
    # gen classes of delta relation
    while leaves:
        a = leaves[0]       
        st = set([a])
        for b in leaves:
            if not smalldeltafun(n.root.c[0],n.root.c[1],a,b):
                st.add(b)       
        for b in st:
            leaves.remove(b)
        cabs.append( frozenset(s.label for s in st))

    for c in n.root.c:
        netsplit = frozenset(get_tree_leaflabels(c))
        yield next(filter(lambda x: netsplit.intersection(x), cabs), frozenset())

def phiSupportsRootLevelcol(network):
    return list( frozenset(c) for c in phiSupportsRootLevel(network))

def phiSupportsRec(network, gtlabels, level = 0):
    """
    Returns a list of triples (Network, GS, Level):
    Network - network 
    GS - inferred largests supports
    Level - how many reticulations removed from the original network
    """

    if not gtlabels.issubset(network.root.visiblelabels()): return [] # required  L(G) subseteq L(N)
    if len(gtlabels)<=1: return [] # phiSupports require at least two labels

    def checkgtlabels(net):

        while True:
            dc = phiSupportsRootLevelcol(net)
            if len(dc) == 2:
                a,b = dc
                if a.intersection(gtlabels) and b.intersection(gtlabels):
                    # proper delta class
                    return True, dc, net
            l1 = frozenset(net.root.c[0].visiblelabels())
            l2 = frozenset(net.root.c[1].visiblelabels())
            if l1.intersection(l2):
                # children labels overlap; no compatible solution
                return False, dc, net
            if not l1.intersection(gtlabels):
                # unused lft child; cut
                net = Network(str2tree(net.root.c[1].netrepr()))
            elif not l2.intersection(gtlabels):
                # unused rgh child; cut                
                net = Network(str2tree(net.root.c[0].netrepr()))
            else:
                return False, dc, net

    ok, dc, network = checkgtlabels(network)
    
    if ok: 
        return [ (network, dc, level) ]

    # try two variants by removing one reticulation 
    i = network.reticulations[0]  
    n1 = network.contractret(i.retid,True)
    n2 = network.contractret(i.retid,False)
    n1o = Network(str2tree(n1))
    n2o = Network(str2tree(n2))
    res = phiSupportsRec(n1o, gtlabels, level+1 ) + phiSupportsRec(n2o, gtlabels, level+1 )

    return res

def phiSupports2str(dc):
    return ";".join( ",".join(str(s) for s in c) for c in dc)

def ppdc(dc):
    return "|".join( "".join(str(s) for s in c) for c in dc)


def phiSupports(network, gtrees, rootlevelonly = False):
    """
    Main function to compute Phi supports from collections of gtrees (may be empty)
    Yields triples (Net, Supports, Level)

    rootlevelonly=True ignores gene trees; 
    """

    if not gtrees or rootlevelonly: 
        yield network, phiSupportsRootLevel(network), 0
    else:
        for gtree in gtrees:
            for r in phiSupportsRec(network, gtree.root.cluster):
                yield r                



