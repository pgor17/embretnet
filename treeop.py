from __future__ import annotations

import sys
from functools import reduce
from random import randint, shuffle
from typing import Set
from itertools import product

def gendotfile(lst, fn, addnodeidcomments):
    f = open(fn, "w")
    f.write("digraph GN {\n")
    for i, t in enumerate(lst):    
        if t:
            t.todotfile(f, f"t{i}t", addnodeidcomments=addnodeidcomments)
    f.write("\n}\n")
    f.close()


def compcostsmp(g, s, c):
    g.set_lca_mapping(s)
    if c == 'D':
        return g.dupcost(s)
    if c == 'L':
        return g.losscost(s)
    if c == 'U':
        return g.duplosscost(s)
    if c == 'C':
        return g.dccost2(s)
    if c == 'c':
        return g.dccost(s)
    if c == 'R':
        return g.rfcost(s)
    raise Exception("Unknown cost " + c)


def ftread(a, oclass):
    try:
        return [oclass(str2tree(l)) for l in open(a, 'r') if l.strip() and l.strip()[0] != '#']
    except Exception:
        try:
            return [oclass(str2tree(x.strip())) for x in a.split(";") if x.strip()]
        except Exception:
            print(f"Nested parenthesis strings or a file name expected in <{a}>",file=sys.stderr)
            sys.exit(-1)

# string -> list with labels
def tokenizer(s):
    tok = []
    i = 0
    while i < len(s):
        if s[i] in "(),:":
            tok.append(s[i])
            i += 1
        elif s[i].isspace():
            i += 1
        elif s[i].isalnum() or s[i] in "#.+-?":
            st = i
            while i < len(s) and (s[i].isalnum() or s[i] in "#.-+?"):
                i += 1
            tok.append(s[st:i])
        elif s[i] == '[':  # comment
            com = ''
            st = i
            while i < len(s) and s[i] != ']':
                i += 1
            i += 1
            tok.append(s[st:i])
        else:
            raise Exception("Non-alphanumeric character: <%s>" % s[i])
    return tok


def str2tree(s):
    tok = tokenizer(s)

    def _st(tok):
        if not tok:
            raise Exception("String too short..." % s)
        if tok[0] == '(':
            tok.pop(0)
            c = [_st(tok)]

            while tok and tok[0] == ",":
                tok.pop(0)
                c.append(_st(tok))

            if tok[0] != ')':
                raise Exception(") expected found <%s>" % tok[0])
            tok.pop(0)

        else:
            c = []

        # label and :...
        l = []
        while tok and tok[0] not in "(),":
            l.append(tok.pop(0))
        return c, l

    if not tok:
        raise Exception("Syntax error. Tokens left: <%s>" % tok)
    return _st(tok)


def node2label(n):
    return str(n).replace("(", " ").replace(")", " ").replace(",", " ")


class Node:
    def __init__(self, tup, par):
        
        self.src, self.labels = tup
        self.parent = par
        self.reticulation = 0  # default value, will be updated in the network constructor

        if self.parent:
            self.height = self.parent.height + 1
        else:
            self.height = 0
        if self.src:
            self.c = [Node(t, self) for t in self.src]
        else:
            self.c = []
        self.interval = None                

        # set attributes from dict
        # see unfold in netop
        if self.labels and type(self.labels[-1])==dict:
            for k,v in self.labels[-1].items():
                setattr(self, k, v)
            self.labels.pop(-1)

        self._setcluster()

        self.comments = [c[1:-1] for c in self.labels if type(c)==str and c[0] == '[']  # newick comments

        self.branchlengthset = False

        # branch lengths
        if ":" in self.labels:
            p = self.labels.index(":")
            if p + 1 < len(self.labels):
                try:
                    self.branchlength = float(self.labels[p + 1])
                except ValueError:
                    print(f"A number expected. Found: <{self.labels[p + 1]}>", file=sys.stderr)
                    sys.exit(-1)

                self.branchlengthset = True

    def _setcluster(self):
        if self.labels and self.labels[0][0] != '[':
            self.label = self.labels[0]
        else:
            self.label = ''

        if self.leaf():
            self.cluster = frozenset([self.label])
            self.clusterleaf = self.label
        else:
            self.clusterleaf = None
            self.cluster = frozenset().union(*(t.cluster for t in self.c))

    def leaf(self):
        return not self.c

    def is_tree_node(self):        
        return not self.leaf() and not self.reticulation

    def setinterval(self, s, distornode):
        if isinstance(distornode, int):
            self.interval = [s, s.ancestor(distornode)]
        else:
            self.interval = [s, distornode]

    def ancestor(self, dist):
        if dist == 0 or not self.parent:
            return self
        return self.parent.ancestor(dist - 1)

    def __str__(self):
        if self.leaf():
            return self.label
        return "(" + ",".join(str(c) for c in self.c) + ")" + self.label

    def netrepr(self):
        if self.leaf():
            return self.clusterleaf
        s = ''
        if self.reticulation:
            s = '#' + self.retid
            # Make sure child of reticulation vertex is processed once
            if hasattr(self, 'visited'):
                del self.visited
                return s
            self.visited = True
        return '(' + ",".join(c.netrepr() for c in self.c) + ')' + s

    def _minlabel(self):
        if not hasattr(self, "minlabel"):

            if self.leaf():
                self.minlabel = self.clusterleaf

            elif self.reticulation:
                self.minlabel = self.c[0]._minlabel()
            else:
                for c in self.c: c._minlabel()

                if all(c.reticulation for c in self.c):
                    self.minlabel = min(c.minlabel for c in self.c)
                else:
                    self.minlabel = min(c.minlabel for c in self.c if not c.reticulation)

        return self.minlabel

    def _netreprsorted(self):

        if self.leaf():
            return self.clusterleaf

        if self.reticulation:
            s = "#" + self.retid
            if hasattr(self, 'visited'):
                del self.visited
                return s
            r = self.c[0]._netreprsorted()
            self.visited = True
            return '(' + r + ')' + s

        ch = sorted(self.c[:], key=lambda c: c.minlabel)

        return '(' + ",".join(c._netreprsorted() for c in ch) + ')'

    def netreprsorted(self):
        """
            Returns a triple: string_repr; should be called for the root only
        """

        self._minlabel() # compute all minlabels

        return self._netreprsorted()



    def contractret(self, reticulation, preserveleftedge, src):
        """
        Netrepr with contracted one reticulation edge
        """
        if self.leaf():
            return self.clusterleaf
        s = ''
        if self.reticulation:
            if self.retid == reticulation:
                if preserveleftedge and self.lftparent == src:
                    return self.c[0].contractret(reticulation, preserveleftedge, self)
                if not preserveleftedge and self.rghparent == src:
                    return self.c[0].contractret(reticulation, preserveleftedge, self)
                return None

            s = '#' + self.retid

            # Make sure child of reticulation vertex is processed once
            if hasattr(self, 'visited'):
                del self.visited
                if hasattr(self, 'retdeleted'):
                    del self.retdeleted
                    return None
                return s
            self.visited = True

        if len(self.c) == 2 and self.c[0] == self.c[1] and self.c[0].reticulation and self.c[0].retid == reticulation: # loop
            return self.c[0].contractret(reticulation, preserveleftedge, self)

        ch = [ c.contractret(reticulation, preserveleftedge, self) for c in self.c ]
        if None in ch:
            # One or more children are contracted
            # print (self, self.reticulation, ch)
            ch.remove(None)
            if len(ch)==0:
                if self.reticulation:
                    self.retdeleted = True
                    return None
                else:
                    raise Exception("Tree child network expected in contractnet")

            if len(ch)==1:
                return ch[0]

        return '(' + ",".join(ch) + ')' + s


    def contractbyleaves(self, labels, keep, src):
        """
        Netrepr with removed leaves
        """
        if self.leaf():
            if keep and self.clusterleaf not in labels or not keep and self.clusterleaf in labels:
                return None
            return self.clusterleaf

        s=''
        ch = [ cs for c in self.c if (cs:=c.contractbyleaves(labels, keep, self)) is not None ]
        if len(ch) != len(self.c):
            # None is present
            if not len(ch):
                return None
            if len(ch)==1:
                return ch[0]
        else:
            if self.reticulation:
                s = '#' + self.retid
                if src == self.lftparent:
                    return s

        return '(' + ",".join(c for c in ch) + ')' + s


    def findredundantreticulation(self):
        if self.leaf(): return None

        if len(self.c)==2:
            c1, c2 = self.c
            for i in range(2):
                if c1.reticulation:
                    if c1.lftparent == c2 or c1.rghparent == c2 or c1.lftparent == c1.rghparent:
                        return c1.retid
                c1, c2 = c2, c1

        for c in self.c:
            if (res:=c.findredundantreticulation()) is not None:
                return res

        return None

    def __repr__(self):
        return str(self)

    def nodes(self):  # preorder
        if self.leaf():
            return [self]
        return sum((t.nodes() for t in self.c), [self])

    def nodes_postorder(self):
        if self.leaf():
            return [self]
        return sum((t.nodes_postorder() for t in self.c), []) + [self]


    def unique_nodes(self):
        return set([node for node in self.nodes()])

    def leaves(self):
        if self.leaf():
            return [self]
        return sum((t.leaves() for t in self.c), [])

    def visiblelabels(self):
        if self.leaf():
            return [ self.label ]
        return sum((t.visiblelabels() for t in self.c), [])

    """ add all nodes reachable from self to d """
    def reachablenodes(self, d):
        if self in d: return d
        d.add(self)
        for c in self.c:
            if c not in d:
                c.reachablenodes(d)
        return d

    # x = self or x below sel.
    def geq(self, x):
        while x:
            if x == self:
                return True
            x = x.parent
        return False

    def leq(self, x):
        return x.geq(self)

    # added so Python3 can sort nodes
    def __lt__(self, x):
        return x.geq(self) and x != self

    def comparable(self, x):
        return self.geq(x) or self.leq(x)

    def lca(self, y):
        a, b = self, y
        if a.height > b.height:
            a, b = b, a
        # a.h <= b.h
        while b.height != a.height:
            b = b.parent

        # a.h == b.h
        while True:
            if a == b:
                return a
            a = a.parent
            b = b.parent

    def findnode(self, cluster):
        if self.cluster == cluster:
            return self
        if self.leaf():
            return None
        for c in self.c:
            r = c.findnode(cluster)
            if r:
                return r
        return None

    # Helper functions used in DP calculation
    def left_reticulation_used(self):
        return 2 ** (self.retnum * 2 - 1)

    def right_reticulation_used(self):
        return 2 ** (self.retnum * 2)

    def get_node_retusage(self, retusage):
        val = (retusage & self.left_reticulation_used()) + (retusage & self.right_reticulation_used())
        val //= self.left_reticulation_used()
        return val

    def treecluster(self, allowed_labels) -> Set[str]:
        """ Returns the set of labels of leaves that are visible from self using non-reticulation edges.
        """
        if self.reticulation:
            return set()
        elif self.leaf():
            return {self.label} if self.label in allowed_labels else set()
        else:
            return self.c[0].treecluster(allowed_labels) | self.c[1].treecluster(allowed_labels)

    def findlcabycluster(self, cluster):        
        if cluster.issubset(self.cluster):
            if self.leaf():
                return self
            for c in self.c:
                if (c:=c.findlcabycluster(cluster)):
                    return c
            return self
        return None



class Tree:
    def __init__(self, tup):
        self.srclist = None

        if isinstance(tup, list):
            self.srclist = tup
            tup = self.srclist[0]
            for t in self.srclist[1:]:
                tup = (tup, t)

        self.root = Node(tup, None)
        self.nodes = self.root.nodes()
        for i, n in enumerate(self.nodes):
            n.num = i
            n.artificial = False
        self.src = tup

        if self.srclist:
            l = self.root
            for i in range(len(self.srclist) - 1):
                l.artificial = True
                l = l.l

    def leaves(self):
        return self.root.leaves()

    def dccost(self, stree):
        c = 0
        for n in self.nodes:
            if n != self.root:
                c += n.lcamap.height - n.parent.lcamap.height - 1
        return c

    def dccost2(self, stree):
        c = 0
        for n in self.nodes:
            if n != self.root:
                c += n.lcamap.height - n.parent.lcamap.height
        return c

    def rfcost(self, stree):
        c = 0
        for n in self.nodes:
            if n != self.root and not n.leaf():
                c += 2*(n.lcamap.cluster != n.cluster)
        return c

    def rfcost2(self, stree):
        return len(self.clusters().symmetric_difference(stree.clusters()))

    def internal_node_nr(self):
        c = 0
        for n in self.nodes:
            if not n.leaf() or n.parent is None:
                c += 1
        return c

    def rfcost_norm(self, stree):
        self.set_lca_mapping(stree)
        if self.root.cluster != stree.root.cluster:
            raise ValueError(f"Non-bijective trees: {self} and {stree}")
        return self.rfcost(stree) / (2 * self.internal_node_nr())

    def dupcost(self, stree):
        d = 0
        for n in self.nodes:
            if n.leaf():
                continue
            for c in n.c:
                if c.lcamap == n.lcamap:
                    d += 1
                    break
        return d

    def splits(self):
        return [(g.l.cluster, g.r.cluster) for g in self.nodes if not g.leaf()]

    def gencost(self, stree, lambdaf, gammaf):
        sleaves = stree.root.cluster
        gsplits = self.splits()
        ssplits = stree.splits()
        c = sum(lambdaf(a, b, frozenset(s)) for s in sleaves for a, b in gsplits)
        i = sum(gammaf(a, b, x, y) for a, b in gsplits for x, y in ssplits)
        return c + i

    def duplosscost(self, tree):
        return self.dccost(tree) + 3 * self.dupcost(tree)

    def losscost(self, tree):
        return self.dccost(tree) + 2 * self.dupcost(tree)

    def findnodeplus(self, cluster):
        if len(cluster) > 2 and cluster[-2] == "+":
            up = int(cluster[-1])
            cluster = cluster[:-2]
        elif len(cluster) > 1 and cluster[-1][0] == "+":
            up = int(cluster[-1])
            cluster = cluster[:-1]
        else:
            up = 0

        s = self.root.findnode(frozenset(cluster))
        if not s:
            return s

        while up and s.parent:
            s = s.parent
            up = up - 1
        return s

    def __str__(self):
        return str(self.root)

    def set_lca_mapping(self, st):
        def comp_lca_map(n):
            if n.leaf():
                clu = n.clusterleaf
                n.lcamap = None
                while clu and not n.lcamap:
                    n.lcamap = st.root.findnode(frozenset([clu]))
                    clu = clu[:-1]

                if not n.lcamap:
                    raise Exception("Lca mapping not found for ", n)
            else:
                # internal
                n.lcamap = reduce(lambda x, y: x.lca(y), (comp_lca_map(c) for c in n.c))
            return n.lcamap

        comp_lca_map(self.root)

    def __repr__(self):
        return repr(self.root)

    def minlcaster(self, cluster):
        c = set(cluster)
        for n in self.root.nodes_postorder():
            if c.issubset(set(n.cluster)):
                return n
        return None

    def clusters(self):
        return set(n.cluster for n in self.nodes)

    def is_lower(self, cluster1: Set, cluster2: Set):
        """ Returns the information whether cluster1 has a lower LCA than cluster2.
            Assumes that cluster1 and cluster2 have a non-empty intersection (i.e. their LCAs are comparable)
        """
        assert cluster1.intersection(cluster2)
        for node in self.root.nodes_postorder():
            is_c1_subset, is_c2_subset = cluster1.issubset(node.cluster), cluster2.issubset(node.cluster)
            if is_c1_subset and not is_c2_subset:
                return True
            elif is_c2_subset:
                return False

    def is_binary(self):
        for node in self.nodes:
            if not len(node.c) in [0, 2]:
                return False
        return True

    def todotfile(self, f, nodeprefix='g', addnodeidcomments=True):
        for n in self.nodes:
            comments = " ".join(n.comments)
            if comments:
                comments = "\n" + comments

            if addnodeidcomments:
                nodeidcom=f" {n.num}"+comments
            else:
                nodeidcom=""                
            s = ''
            if n.leaf():
                s = str(n)
            f.write("%s%d [label=\"%s%s\"];\n" % (nodeprefix, n.num, s, nodeidcom))

        for n in self.nodes:
            if n.parent:
                f.write("%s%d -> %s%d;\n" % (nodeprefix, n.parent.num, nodeprefix, n.num))

    def scenarioenum(self, gtree, network, genalldots=False):

        def ppc(node):
            return "".join(c for c in sorted(node.cluster))

        def rfsim(g, xi):
            """ Returns: cost, mapping to N^, C_xi(g) """
            if g.leaf():
                unleaf = xi[g]
                g.dat = 0, unleaf, unleaf.netsrc.treevisible            
                return g.dat

            cl, ml, tvl = rfsim(g.c[0], xi)
            cr, mr, tvr = rfsim(g.c[1], xi)

            # compute C_xi(g)
            m = ml.lca(mr)

            s=tvl.union(tvr)            
            for i in ml, mr:
                while i!=m:
                    i=i.parent
                    s = s.union(i.netsrc.treevisible)                    

            g.dat = cl+cr+int(s==g.cluster), m, s
            return g.dat

        def ppxi(xi, g, printdetails=False):
            

            if printdetails: 
                for k,v in xi.items():
                    print(f"{k}:{v.num} ",end="")                                
                print()

            res = {}
            for n in gtree.nodes:
                rfs, ximap, xicluset = n.dat
                xiclu = ''.join(sorted(xicluset))
                lcagxicluset = gtree.root.findlcabycluster(xicluset)

                netsrc = ximap.netsrc

                res[n] = (rfs, ximap, xicluset, xiclu, netsrc)

                
                n.comments[-1] = f"\nxi={ximap.num}\nxicl={xiclu}\nRFs={rfs:2}"                
                # add to N^
                ximap.comments[-1]+= f"\ng={n.num} xicl={xiclu}\nRFs={rfs:2}"                

                # add to N
                netsrc.comments[-1] += f"\ng={n.num} xicl={xiclu}\nRFs={rfs:2}"                

                # aggregate at N
                netsrc.gmaps.setdefault(n,set()).add((xiclu,rfs, lcagxicluset))

                if printdetails:
                    print(f" {n.num}: RFs={rfs:2}",end='')
                    print(f" {''.join(sorted(n.cluster)):7}", end='')                                
                    print(f" m={ximap.num:2} {''.join(sorted(xiclu))}")

            return res


        gtlabels=set(l.label for l in gtree.leaves())
        ld = {}
        for l in self.leaves():
            ld.setdefault(l.label,[]).append(l)        
        # print(ld)
        for s in network.nodes:
            s.treevisible = s.treecluster(allowed_labels=gtlabels)
            #print(">>",s,s.treevisible)

        for g in gtree.nodes:
            g.comments.append("")

        gtree.root.comments.append("")
        for s in self.nodes:
            s.comments.append("")

        for s in network.nodes:
            s.comments.append("")
            s.gmaps = {}
            s.gmapsopt = {}
            s.gmapssuperopt = {}
            s.gmapssuperoptext = {}

        resultdat = []        
        for scenid, scen in enumerate(product( *list(ld[gl.label] for gl in gtree.leaves() ))):
            xi = dict(zip(gtree.leaves(),scen)) # mapping
            for s in self.nodes: s.comments[-1]=""
            for s in network.nodes: s.comments[-1]=""
            
            #print(f"Scen {en}")
            rfsim(gtree.root, xi)
            resultdat.append(ppxi(xi,gtree))
            gtree.root.comments[-2] = f"Scenario {scenid}"

            if genalldots: 
                gendotfile([gtree, self, network], f"xit{scenid}.dot", True)        

        # get opt solutions
        maxrfsim = max( r[gtree.root][0] for r in resultdat)
        optscendat =  [ (scenid,r) for scenid,r in enumerate(resultdat) if r[gtree.root][0]==maxrfsim ]

        
        for _,o in optscendat:
            for n,(rfs, ximap, xicluset, lcagxicluset, xiclu, netsrc) in o.items():
                netsrc.gmapsopt.setdefault(n.num,set()).add((rfs, lcagxicluset))

        status = 0 

        for s in network.nodes:
            for n in s.gmapsopt:
                if len(s.gmapsopt[n])>1:
                    print ("Warning Ambiguous Opt",n,s.gmapsopt[n])
                    status |= 2

        # check correctness
        mingslcamap = {} # from Ci_g()
        for n in network.nodes:
            for g,v in n.gmaps.items():
                
                nmaxrfsim = max(d[1] for d in v)
                minlca = None
                for xiclu, rfsim, glca in v:
                    if rfsim==nmaxrfsim:
                        if not minlca: minlca = glca
                        else: 
                            if glca.leq(minlca): 
                                minlca = glca

                for xiclu, rfsim, glca in v:
                    if rfsim==nmaxrfsim and glca!=minlca:
                        print ("Non-lca", s.num, g, v,"maxrfsim=",nmaxrfsim,"minlca",minlca)
                        status |= 1

                mingslcamap[g,n] = (nmaxrfsim, minlca)


        nreachable = {}
        mingslcamapbelow = {}
        for n in network.nodes:
            nreachable = n.reachablenodes(set())
            for g in gtree.nodes:
                if (g,n) not in mingslcamap: continue

                bestrfs, bestxiglca = mingslcamap[g,n]
                for nbelow in nreachable:
                    if (g,nbelow) not in mingslcamap: continue
                    belowrfs, belowxiglca = mingslcamap[g, nbelow]
                    if belowrfs<bestrfs: continue
                    if belowrfs==bestrfs and belowxiglca.geq(bestxiglca): continue
                    if belowrfs>bestrfs or (belowrfs==bestrfs and belowxiglca!=bestxiglca and belowxiglca.leq(bestxiglca)):
                        bestrfs = belowrfs
                        bestxiglca = belowxiglca

                mingslcamapbelow[g,n] = (bestrfs, bestxiglca)

                if mingslcamapbelow[g,n] != mingslcamap[g,n]:
                    status|=16
                    print(f"minglca below g={g.num} n={n.num}: {mingslcamap[g,n]} -> {mingslcamapbelow[g,n]}")

        # check super-scenario
        superscenarios=[]
        for scenid, o in optscendat:
            for g, (rfs, ximap, xicluset, lcagxicluset, xiclu, netsrc) in o.items():
                if (rfs,lcagxicluset) != mingslcamap[g, netsrc]:
                    break
            else:
                superscenarios.append(scenid)

        # check ext-super-scenario
        superscenariosext=[]
        for scenid, o in optscendat:
            for n,(rfs, ximap, xicluset, lcagxicluset, xiclu, netsrc) in o.items():
                if (rfs,lcagxicluset) != mingslcamapbelow[n,netsrc]:
                    break
            else:
                superscenariosext.append(scenid)


        # maps for super-scenarios
        for scenid,o in optscendat:
            if scenid in superscenarios:
                for n,(rfs, ximap, xicluset, lcagxicluset, xiclu, netsrc) in o.items():
                    netsrc.gmapssuperopt.setdefault(n.num,set()).add((rfs, lcagxicluset))

        # maps for ext-super-scenarios
        for scenid,o in optscendat:
            if scenid in superscenariosext:
                for n,(rfs, ximap, xicluset, lcagxicluset, xiclu, netsrc) in o.items():
                    netsrc.gmapssuperoptext.setdefault(n.num,set()).add((rfs, lcagxicluset))


        if not superscenarios:
            status = status | 4            
            print ("No super-scenario",gtree, network)

        if not superscenariosext:
            status = status | 32
            print ("No ext-super-scenario",gtree, network)

        for g in gtree.nodes:
            g.comments[-1]=""
        
        for s in self.nodes:
            s.comments[-1]=""

        for s in network.nodes:
            s.comments[-1]=""


        dp,_,_ = network.ret_min_rf(gtree,True)

        if dp!=maxrfsim:
            status |= 8
            print ("Error DP!=scenarioenum", gtree, network)

        if status:
            f = open('xi.log','a')
            f.write(f"{status};{maxrfsim};{len(resultdat)};{len(optscendat)};{gtree};{network}\n")
            f.close()


        optscendatnums =  [ i for i,r in enumerate(resultdat) if r[gtree.root][0]==maxrfsim ]
        #gtree.root.comments[-2] = f" dp={dp}\nmaxrfsim={maxrfsim}\nopt_scenariosnums={','.join(str(i) for i in optscendatnums)}"
        gtree.root.comments[-2] = f" dp={dp}\nmaxrfsim={maxrfsim}\n#optscen={len(optscendatnums)}"
        gtree.root.comments[-2]+= f"\n#superscen={len(superscenarios)}"
        gtree.root.comments[-2]+= f"\n#superscenext={len(superscenariosext)}"
        # final    
        for s in network.nodes:
            s.comments[-1] = "\n".join(f"{k}:{v}" for k,v in s.gmaps.items())

            if s.gmapsopt:
                 s.comments[-1] += "\n=====OPT=====\n" + "\n".join(f"{k}:{v}" for k,v in s.gmapsopt.items())

            if s.gmapssuperopt:
                 s.comments[-1] += "\n=====SUPEROPT=====\n" + "\n".join(f"{k}:{v}" for k,v in s.gmapssuperopt.items())

            # if s.gmapssuperoptext:
            #      s.comments[-1] += "\n=====SUPEROPTEXT======\n" + "\n".join(f"{k}:{v}" for k,v in s.gmapssuperoptext.items())

            mm = "\n".join(f"{g.num}:RF={mingslcamap[g,n][0]},{ppc(mingslcamap[g,n][1])}" for g,n in mingslcamap if n==s)
            if mm:
                s.comments[-1] += "\n======minglca======\n" + mm

            mm = "\n".join(f"{g.num}:RF={mingslcamapbelow[g,n][0]},{ppc(mingslcamapbelow[g,n][1])}" for g,n in mingslcamapbelow if n==s)
            if mm:
                s.comments[-1] += "\n======minglca_below======\n" + mm

            # print (s,s.gmaps)

        print (f"status={status} maxrfsim={maxrfsim} #scen={len(resultdat)} #superscen={len(optscendat)}")
            
        gendotfile([gtree, self, network], f"xit_fin.dot", True)   

        return status
            


def getlabs(a, z, labels):
    alfsize = z - a + 1
    letters = [chr(a + i) for i in range(alfsize)]
    return (letters[i % len(letters)] + ("" if i < alfsize else str(i // alfsize)) for i in range(labels))


def randtreestrfromlist(p):
    # gen random tree with all subtrees
    while len(p) > 1:
        a = p.pop(randint(0, len(p) - 1))
        b = p.pop(randint(0, len(p) - 1))
        p.append("(" + a + "," + b + ")")

    return p[0]


def randtreestr(labels, use_numbers=False):
    if use_numbers:
        p = [str(i) for i in range(1, labels + 1)]
    else:
        p = list(getlabs(ord('a'), ord('z'), labels))

    return randtreestrfromlist(p)


def compatiblesets(x, y):
    return (x.issubset(y) or y.issubset(x)) or not x.intersection(y)


def treestrfromclusters(clusters, binarytree=1):
    """
    Return a tree string from a set of compatible clusters
    Assumption: all trivial clusters are present (root, leaves)
    Multifurcations will be randomly resolved if binarytree=1

    Args:
        clusters: a set of clusters (sets of labels)
        binarytree: flag indicating that the tree must be binary
    """

    clusters = sorted(clusters, key=lambda x: -len(x))
    tree = {}  # cluster -> its subtree
    while clusters:
        n = clusters.pop()

        if len(n) == 1:
            tree[n] = list(n)[0]  # leaf
        else:
            children = [c for c in tree if c.issubset(n)]  # gen children
            subtrees = [tree[c] for c in children]
            if binarytree:
                tree[n] = randtreestrfromlist(subtrees)  # binarize randomly
            else:
                tree[n] = "(" + ",".join(subtrees) + ")"  # allow mutlifurcations

            for c in children:
                del tree[c]  # remove used subtrees

    return tree.pop(n)


def quasiconsensusgen(gtrees, qtreescnt):
    """
    Generate quasi-consesus tree strings from a set of gene trees

    Args:
        gtrees: a collection of gene trees
        qtreescnt: how many quasi-consensus tree to generate
    """

    d = {}
    alllabels = set()
    for g in gtrees:
        for n in g.nodes:            
            d[n.cluster] = d.setdefault(n.cluster, 0) + 1
            if len(n.cluster) == 1:
                alllabels = alllabels.union(n.cluster)
    alllabels = frozenset(alllabels)

    counts = [[] for i in range(1 + max(d.values()))]
    for k in d:
        counts[d[k]].append(k)

    for _ in range(qtreescnt):
        curclusters = set([alllabels])
        for q in reversed(counts):
            shuffle(q)  # some randomness
            for c in q:
                if all(compatiblesets(c, cc) for cc in curclusters):
                    curclusters.add(c)

        # compatible clusters are generated
        yield treestrfromclusters(curclusters)
