import argparse

import matplotlib.pyplot as plt

plt.style.use('seaborn-v0_8')

from treeop import Tree, str2tree, randtreestr
from netsearch import get_init_nets, supernet_search
from random import seed


def random_tree(n):
    return Tree(str2tree(randtreestr(n)))


def plot_stats(args, stats_keys, experiments, param_key, x_range, x_label, param_name):
    plt.figure(figsize=(10, 7.5))
    suptitle = "PARAMETERS: "
    if param_name != 'r':
        suptitle += f"r={args.r}, "
    if param_name != 'm':
        suptitle += f"m={args.m}, "
    if param_name != 'n':
        suptitle += f"n={args.n}, "
    if param_name != 'k':
        suptitle += f"k={args.k}"
    plt.suptitle(suptitle)
    for i, stats_key in enumerate(stats_keys):
        plt.subplot(2, 2, i + 1)
        plt.title(stats_key.upper())
        for key, val in experiments.items():
            plt.plot(x_range, val[param_key][stats_key], '-o', label=key)
        plt.xlabel(x_label)
        plt.ylabel(stats_key)
        plt.ylim(0)
        plt.xticks(x_range)
        plt.legend(loc='lower right', fontsize='x-large', frameon=True, facecolor='white', edgecolor='black')
    plt.tight_layout()
    plt.savefig(f"{args.f}{param_name}.pdf")


def main():
    example_txt = """example:
    python3 test_hc_stats.py -r2 -m2 -n5 -k1 --r_list 1,2,3 --m_list 1,2,3 --n_list 4,5,6 --k_list 0,1,2
    """

    desc = """Run netsearch runtime statistics on random networks.
    Fixes given parameters and changes one parameter at a time.
    """

    parser = argparse.ArgumentParser(description=desc, epilog=example_txt,
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-f", help="Target filename prefix to save .pdf reports", type=str, default="hc_stats_")
    parser.add_argument("-r", help="Number of initial networks", type=int, default=15)
    parser.add_argument("-m", help="Number of gene trees", type=int, default=5)
    parser.add_argument("-n", help="Number of leaves", type=int, default=10)
    parser.add_argument("-k", help="Number of reticulations", type=int, default=2)
    parser.add_argument('--r_list', help="delimited list of all 'r' parameters to check",
                        type=lambda s: [int(item) for item in s.split(',')], default='1,2,5,15,25')
    parser.add_argument('--m_list', help="delimited list of all 'm' parameters to check",
                        type=lambda s: [int(item) for item in s.split(',')], default='1,2,3,5,10')
    parser.add_argument('--n_list', help="delimited list of all 'n' parameters to check",
                        type=lambda s: [int(item) for item in s.split(',')], default='4,5,6,7,8,9,10,11,12')
    parser.add_argument('--k_list', help="delimited list of all 'k' parameters to check",
                        type=lambda s: [int(item) for item in s.split(',')], default='0,1,2,3,4')
    parser.add_argument("--verbose", help="Print progress bars (1-print, 0-silent)", type=int, default=1)
    parser.add_argument("--seed", help="Seed for all random operations", type=int, default=1)
    args = parser.parse_args()

    if args.r < 1:
        print(f"Argument 'r' too low ({args.r}), setting to 1")
        args.nmin = 1
    if args.m < 1:
        print(f"Argument 'm' too low ({args.m}), setting to 1")
        args.nmin = 1
    if args.n < 3:
        print(f"Argument 'n' too low ({args.n}), setting to 3")
        args.n = 3
    if args.k < 0:
        print(f"Argument 'k' too low ({args.k}), setting to 0")
        args.k = 0
    if args.k > args.n - 2:
        print(f"Error: k ({args.k}) can't be greater than n-2 ({args.n - 2})")
        exit(1)
    if any([x > args.n - 2 for x in args.k_list]):
        print(f"Error: elements of k_list ({args.k_list}) can't be greater than n-2 ({args.n - 2})")
        exit(1)
    if any([x < args.k + 2 for x in args.n_list]):
        print(f"Error: elements of n_list ({args.n_list}) can't be smaller than k+2 ({args.k + 2})")
        exit(1)

    seed(args.seed)
    trees = {n: [random_tree(n) for _ in range(max(args.m_list))] for n in args.n_list}
    nets_rand = {n: {k: get_init_nets(
        trees[n], k, max(args.r_list), 'random') for k in [x for x in args.k_list if x <= n - 2]} for n in args.n_list}
    nets_cons = {n: {k: get_init_nets(
        trees[n], k, max(args.r_list), 'quasi-cons') for k in [x for x in args.k_list if x <= n - 2]} for n in
        args.n_list}
    experiments = {'rand': {'nets': nets_rand}, 'cons': {'nets': nets_cons}}
    stats_keys = ['best_cost', 'total_time', 'avg_steps', 'avg_moves']
    param_keys = ['rlist', 'mlist', 'nlist', 'klist']
    for e in experiments.values():
        for param_key in param_keys:
            e[param_key] = {stats_key: [] for stats_key in stats_keys}

    if args.verbose:
        print("GETTING STATS FOR [R]", flush=True)
    for r in args.r_list:
        for e in experiments.values():
            stats = supernet_search(trees[args.n][:args.m], e['nets'][args.n][args.k][:r], verbose=args.verbose)
            for stats_key in stats_keys:
                e['rlist'][stats_key].append(stats[stats_key])
    plot_stats(args, stats_keys, experiments, 'rlist', args.r_list, "Number of networks (r)", "r")

    if args.verbose:
        print("GETTING STATS FOR [M]", flush=True)
    for m in args.m_list:
        for e in experiments.values():
            stats = supernet_search(trees[args.n][:m], e['nets'][args.n][args.k][:args.r], verbose=args.verbose)
            for stats_key in stats_keys:
                e['mlist'][stats_key].append(stats[stats_key])
    plot_stats(args, stats_keys, experiments, 'mlist', args.m_list, "Number of trees (m)", "m")

    if args.verbose:
        print("GETTING STATS FOR [N]", flush=True)
    for n in args.n_list:
        for e in experiments.values():
            stats = supernet_search(trees[n][:args.m], e['nets'][n][args.k][:args.r], verbose=args.verbose)
            for stats_key in stats_keys:
                e['nlist'][stats_key].append(stats[stats_key])
    plot_stats(args, stats_keys, experiments, 'nlist', args.n_list, "Number of leaves (n)", "n")

    if args.verbose:
        print("GETTING STATS FOR [K]", flush=True)
    for k in args.k_list:
        for e in experiments.values():
            stats = supernet_search(trees[args.n][:args.m], e['nets'][args.n][k][:args.r], verbose=args.verbose)
            for stats_key in stats_keys:
                e['klist'][stats_key].append(stats[stats_key])
    plot_stats(args, stats_keys, experiments, 'klist', args.k_list, "Number of reticulations (k)", "k")


if __name__ == "__main__":
    main()
