from netop import Network
from treeop import str2tree, Tree, compcostsmp

network_str = "(((((((t10)#I,t3),#E))#B,(((t11)#H,(((#G,t4))#E,t12)),(t1)#C)),#B),((((t9)#F,t2))#D,((#C,(#H,(#A,(((t6,(t5)#A),(#F,((t8)#G,t7))),#I)))),#D)))"
tree_str = "(((t9,t2),(t8,(t7,(t6,t5)))),((t4,(t3,t10)),(t1,(t12,t11))))"

network = Network(str2tree(network_str))
tree = Tree(str2tree(tree_str))
cost = network.branch_and_bound(tree, cost_type="R")
_, score, usage = network.ret_min_rf(tree)
_, score_correct, usage_correct = network.ret_min_rf_fast(tree)
print(f"Score: {score}, expected: {score_correct}")
print(f"Usage equal: {usage == usage_correct}. Usage: {usage}, expected: {usage_correct}")
print(f"Cost branch and bound {cost}")
min_cost = float("inf")
for t in network.displayedtrees():
    curr_cost = compcostsmp(tree, Tree(str2tree(t)), "R")
    min_cost = min(min_cost, curr_cost)
print(f"RF cost: {min_cost}")
