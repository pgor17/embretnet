import multiprocessing as mp
import re
import time
from random import shuffle, seed

from tqdm import tqdm

import NetworkContainment
import RandomNetworks
from netop import Network, addretstr
from treeop import Tree, str2tree, quasiconsensusgen, compcostsmp


def not_comp_msg():
    return "Networks not comparable! Please check assumptions."


def get_sibling(node, parent):
    """Get the other child of parent. Parent has to be a tree node."""
    return parent.c[int(parent.c[0] == node)]


def update_child(node, old, new):
    """Changes the child or one of children for a tree node or reticulation."""
    if old in node.c:
        node.c = [new if x == old else x for x in node.c]
    else:
        raise Exception("Node doesn't have given child, can't update!")


def update_parent(node, old, new):
    """Changes the parent or one of parents for a tree node or reticulation."""
    no_par_msg = "Node doesn't have given parent, can't update!"
    if node.reticulation:
        if node.lftparent == old:
            node.lftparent = new
        elif node.rghparent == old:
            node.rghparent = new
        else:
            raise Exception(no_par_msg)
    elif node.parent == old:
        node.parent = new
    else:
        raise Exception(no_par_msg)


def are_comparable(net1, net2):
    """
    Check if two networks can be compared for inclusion using our algorithms.

    Args:
        net1: object of class Network
        net2: object of class Network
    Returns:
        bool for satisfying conditions
    """

    # check if nets are in proper class
    if not net1.treechild() or not net2.treechild():
        return False

    # check if nets are bijectively labeled by the same set
    if set(net1.get_labels()) != set(net2.get_labels()):
        return False

    return True


def contains_subnetwork(net, subnet):
    """
    Check if 'subnet' is contained within 'net'.
    Works only for binary (originally semi-binary) tree-child networks.
    Works only for bijective labeling.
    Works in O(n+k) time where n is #leaves and k is #reticulations.

    Args:
        net: object of class Network,
        subnet: object of class Network
    Returns:
        bool for satisfying conditions
    """

    if not are_comparable(net, subnet):
        raise Exception("Networks not comparable! Please check assumptions.")

    # Use the outer program (to do: incorporate program into the script).
    # Currently we need to reinitialize networks and change between two
    # input formats, which is inefficient.
    net_repr = net.root.netrepr()
    subnet_repr = subnet.root.netrepr()
    net_ret_names = {x[0] for x in net_repr.split('#')[1:]}
    subnet_ret_names = {x[0] for x in subnet_repr.split('#')[1:]}
    ret_names = net_ret_names | subnet_ret_names
    ret_names = sorted(list(ret_names))
    for i, ret_name in enumerate(ret_names):
        net_repr = net_repr.replace(ret_name, f'H{i}')
        subnet_repr = subnet_repr.replace(ret_name, f'H{i}')

    net_repr = re.sub(r"([a-z]+)", r"'\1'", net_repr) + ';'
    subnet_repr = re.sub(r"([a-z]+)", r"'\1'", subnet_repr) + ';'

    return NetworkContainment.main(net_repr, subnet_repr)


def is_isomorphic(net1, net2):
    """
    Check if two networks are isomorphic.
    Works only for binary (originally semi-binary) tree-child networks.
    Works only for bijective labeling.
    It works in O(n+k) time where n is #leaves and k is #reticulations.

    Args:
        net1: object of class Network
        net2: object of class Network
    Returns:
        bool for satisfying conditions
    """

    if not are_comparable(net1, net2):
        raise Exception("Networks not comparable! Please check assumptions.")

    # check the basic isomorphism invariants
    if len(net1.nodes) != len(net2.nodes):
        return False

    if len(net1.reticulations) != len(net2.reticulations):
        return False

    # check if net1 contains net2
    if not contains_subnetwork(net1, net2):
        return False

        # second inclusion should be satisfied by the invariants check
    return True


def random_treechild_network(leaves, reticulations):
    """
    Return a random binary tree-child network with n leaves and k reticulations.
    Uses tree-child sequences as a method to generate proper networks.
    All possible networks have positive probability of being generated.
    Has to satisfy k <= n-2 to create networks with good properties.

    Args:
        leaves: int, size of taxon set or fixed-length iterable object of str
            values encoding labels
        reticulations: int, number of reticulate nodes
    Returns:
        object of class Network
    """

    k = reticulations
    n = len(leaves) if hasattr(leaves, '__iter__') else leaves

    if k > n - 2:
        msg = f"Too many reticulations (n={n}, k={k})!"
        raise Exception(msg)

    # Use the outer program (TODO: incorporate program into the script).
    # Currently we need to reinitialize networks and change between two
    # input formats, which is unefficient.
    net_newick = False
    while not net_newick:
        net_newick = RandomNetworks.main(n, k, binary=True)

    net = Network(str2tree(net_newick.replace("'", "").replace(";", "")))

    # if labels are given in list, we have to update net
    if hasattr(leaves, '__iter__'):

        for leaf, new_label in zip(net.get_leaves(), leaves):
            leaf.clusterleaf = new_label
        net = Network(str2tree(net.root.netrepr()))

    return net


def get_movable_edges(net, relaxed=False):
    """
    For a given binary network, get a list of edges, whose tails are movable
    using tree-child tail moves.
    See the definition of tree-child tail moves in the paper for more details.

    Args:
        net: object of class Network
        relaxed: bool, whether to relax condition 3b
    Returns:
        list of 2-tuples with parent-child node pairs from net
    """

    # we start by defining all edge tails (inner tree nodes only allowed)
    all_tails = net.get_inner_tree_nodes()
    movable_edges = []
    for u in all_tails:

        # we can always take root edges
        if u == net.root:
            movable_edges.extend((u, v) for v in u.c)

        # condition 3b from definition
        elif not relaxed and u.parent.reticulation:
            if not u.c[1].reticulation:
                movable_edges.append((u, u.c[0]))
            if not u.c[0].reticulation:
                movable_edges.append((u, u.c[1]))

        else:

            # condition 1b from definition
            if not u.parent.reticulation:  # unnecessary check if not relaxed!
                u_sibling = get_sibling(u, u.parent)
                if u_sibling in u.c:
                    movable_edges.append((u, u_sibling))

                # if nothing is wrong then we can add both edges
                else:
                    movable_edges.extend((u, v) for v in u.c)
            else:
                movable_edges.extend((u, v) for v in u.c)  # unnecessary check if not relaxed!

    return movable_edges


def get_original_edge(net, movable_edge):
    """
    For a given binary network and movable edge, get a target edge needed to
    perform a reversed tail move.

    Args:
        net: object of class Network
        movable_edge: 2-tuple with parent-child node pair from net
    """

    u, v = movable_edge
    v_sibling = get_sibling(v, u)

    if u == net.root:
        return v_sibling, v_sibling
    return u.parent, v_sibling


def get_target_edges(net, movable_edge, time_consistent=False):
    """
    For a given binary network and movable edge, get a list of edges, that it
    can move to, using a tree-child tail move. See the definition of tree-child 
    tail moves in the paper for more details. Can be limited to time consistent
    moves for more biologically relevant networks.

    Args:
        net: object of class Network
        movable_edge: 2-tuple with adjacent nodes from net
        time_consistent: bool, whether networks suitable for HGT model
    Returns:
        list of 2-tuples with parent-child node pair from net
    """

    u, v = movable_edge
    v_sibling_u = get_sibling(v, u)
    condition_3c = False

    # condition 3c from definition
    if not u == net.root and not u.parent.reticulation:
        u_sibling = get_sibling(u, u.parent)
        if u_sibling.reticulation and v_sibling_u.reticulation:
            condition_3c = True
            target_edges = [(u.parent, u_sibling)]

    if not condition_3c:
        # first get all edges and add a dummy root pendant edge
        all_tails = net.get_inner_nodes()
        target_edges = [(s, t) for s in all_tails for t in s.c]
        target_edges.append((net.root, net.root))

        # remove edges below movable edge
        tails_below = [n for n in v.nodes() if not n.leaf()]
        edges_below = [(s, t) for s in tails_below for t in s.c]
        target_edges = [edge for edge in target_edges if not edge in edges_below]

        # remove edge itself plus adjacent edges (condition 2a from definition)
        cur_edges = [edge for edge in target_edges if u in edge]
        target_edges = [edge for edge in target_edges if not edge in cur_edges]

        # condtion 3a and 1a from detifition
        if v.reticulation:
            target_edges = [(s, t) for (s, t) in target_edges if not t.reticulation]

        # condition 2b from definition
        if not u == net.root and v in u.parent.c:
            if u.parent == net.root:
                target_edges.remove((u.parent, u.parent))
            else:
                target_edges.remove((u.parent.parent, u.parent))
        if v in v_sibling_u.c:
            target_edges.remove((v_sibling_u, get_sibling(v, v_sibling_u)))

    # extra condition for time consistency
    if time_consistent:
        original_edge = get_original_edge(net, movable_edge)
        for target_edge in list(target_edges):
            tail_move(net, movable_edge, target_edge)
            if not net.istimeconsistent():
                target_edges.remove(target_edge)
            tail_move(net, movable_edge, original_edge)

    return target_edges


class _PendantRoot:
    """Inner class used occasionally for tail moves.
    See `tail_move` description for more detailed information."""

    def __init__(self, root):
        self.c = [root]


def tail_move(net, movable_edge, target_edge):
    """
    For a given binary network and pair of edges, move a tail of the first edge
    to the second one. This implementation simply reattaches tail, but has to
    sometimes attach a dummy pendant root to perform algorithm as in paper.
    Works in-place changing the network (algorithm is easily reversible).

    See Definition 2.6 from "Exploring the Tiers of Rooted Phylogenetic
    Network Space Using Tail Moves" by Remie Janssen et al. (2017).

    Args:
        net: object of class Network
        movable_edge: 2-tuple with parent-child node pair from net
        target_edge: 2-tuple with parent-child node pair from net
    """

    # note: u is a tree node, others can be either tree nodes or reticulations
    u, v = movable_edge
    s, t = target_edge

    # add a pendant root if necessary
    pendant_needed = u == net.root or t == net.root

    if pendant_needed:
        # attach the pendant root above current rooot
        pendant_root = _PendantRoot(net.root)
        net.root.parent = pendant_root
        net.root = pendant_root

        # update target_edge if it was supposed to be a pendant edge
        if s == t:
            s = pendant_root

    # define few variables
    u_parent = u.parent
    v_sibling = get_sibling(v, u)

    # detach node u
    update_child(u_parent, u, v_sibling)
    update_parent(v_sibling, u, u_parent)

    # subdivide new edge
    update_child(s, t, u)
    update_parent(t, s, u)

    # update u to new edge
    update_child(u, v_sibling, t)
    u.parent = s

    # delete pendant root if it exists
    if pendant_needed:
        net.root = pendant_root.c[0]
        net.root.parent = None
        del pendant_root


def tail_moves_gen(net, check_reverse=False, shuffle_moves=True,
                   random_state=None, relaxed=False, time_consistent=False):
    """
    For a given binary network, generate and perform all possible tail moves,
    doing it in-place and yielding edges used to do given transformation.
    Reverses back to original state between yields (correctness of reversing
    can be checked by optional argument). Can limit to a given distance moves.

    Args:
        net: object of class Network
        check_reverse: bool for diagnosing reverse operations
        shuffle_moves: bool for randomizing moves order
        random_state: int for setting seed, works only if shuffle_moves=True
        relaxed: bool, whether to relax condition 3b
        time_consistent: bool, whether networks suitable for HGT model
    Yields:
        movable_edge and target_edge, which are 2-tuples with parent-child node
            pairs from net
    """
    # optional
    if check_reverse:
        net_copy = Network(str2tree(net.root.netrepr()))

    movable_edges = get_movable_edges(net, relaxed=relaxed)
    if shuffle_moves:
        seed(random_state)
        shuffle(movable_edges)

    for i, movable_edge in enumerate(movable_edges):
        original_edge = get_original_edge(net, movable_edge)
        target_edges = get_target_edges(net, movable_edge, time_consistent)
        if shuffle_moves:
            seed(random_state)
            shuffle(target_edges)

        for j, target_edge in enumerate(target_edges):
            tail_move(net, movable_edge, target_edge)
            yield movable_edge, target_edge

            # reverse to original network
            tail_move(net, movable_edge, original_edge)

            # optional
            if check_reverse and net.root.netrepr() != net_copy.root.netrepr():
                raise Exception("Reverse tail move didn't work as expected!")


def get_init_nets(gene_trees, reticulations, nets_number,
                  init_method='quasi-cons', relaxed=False, time_consistent=False):
    """
    Given gene trees, create a given number of initial networks with a fixed
    number of reticulations. With 'quasi-cons' method it generates tree from
    the most common clusters, and then randomly adds reticulations.
    With 'random' method, it simply generates networks at random.
    With 'species_tree' method it takes a species tree string instead of gene 
    trees and adds reticulations.

    Args:
        gene_trees: list of objects of class Tree
        reticulations: int
        nets_number: int
        init_method: str, 'random', 'quasi-cons' or 'species_tree'
        relaxed: bool, whether to relax condition 3b
        time_consistent: bool, whether networks suitable for HGT model
    Returns:
        list of objects of class Network
    """

    if init_method == 'random':
        labels = {leaf.label for tree in gene_trees for leaf in tree.leaves()}
        labels = sorted(list(labels))
        return [random_treechild_network(
            labels, reticulations) for _ in range(nets_number)]

    elif init_method == 'quasi-cons':
        trees = quasiconsensusgen(gene_trees, nets_number)
        net_type = int(relaxed)
        return [Network(str2tree(addretstr(
            t, reticulations, networktype=net_type, time_consistent=time_consistent))) for t in trees]

    elif init_method == 'species_tree':
        species_tree = gene_trees
        net_type = int(relaxed)
        return [Network(str2tree(addretstr(
            species_tree, reticulations, networktype=net_type, time_consistent=time_consistent))) for _ in
            range(nets_number)]

    else:
        raise Exception(f"Wrong method name: '{init_method}'!")


def naive_dc(tree, network, norm_dc=True):
    dc = min([compcostsmp(tree, Tree(str2tree(disp_tree)), 'C') for disp_tree in network.displayedtrees()])
    if norm_dc:
        dc -= 2 * len([x for x in tree.leaves()]) - 2
    return dc


def naive_rf(tree, network):
    return min(compcostsmp(tree, Tree(str2tree(disp_tree)), 'R') for disp_tree in network.displayedtrees())


def _net_hill_climb(params):
    """Inner subroutine for hill climbing for one given network.
    See `supernet_search` for more detailed information."""

    gene_trees, net, fast_loop, shuffle_moves, random_state, relaxed, time_consistent, cost_type, fast_DP, norm_dc = params

    # initialize loop parameters
    # if cost_type == "C":
    mincost = sum(net.branch_and_bound(tree, cost_type=cost_type, fast_DP=fast_DP) for tree in gene_trees)
    
    # elif cost_type == "R":
    #     mincost = sum(naive_rf(tree, net) for tree in gene_trees)
    # else:
    #     raise NotImplementedError(f"Cost {cost_type} not implemented")
    improving, steps_cnt, moves_cnt, start_time = True, 0, 0, time.time()

    # hill climbing
    while improving:
        steps_cnt += 1
        improving = False
        moves_gen = tail_moves_gen(net, shuffle_moves=shuffle_moves,
                                   random_state=random_state, relaxed=relaxed,
                                   time_consistent=time_consistent)

        # check all possible moves from the current state
        for move in moves_gen:
            moves_cnt += 1
            cost = sum(net.branch_and_bound(tree, cost_type=cost_type, fast_DP=fast_DP) for tree in gene_trees) if cost_type == "C" else sum(naive_rf(tree, net) for tree in gene_trees)
            if cost < mincost:
                mincost = cost
                movable_edge, target_edge = move
                improving = True
                if fast_loop:
                    break

        # update net if found a better neighbor
        if improving:
            tail_move(net, movable_edge, target_edge)

    # subtract min cost from cost
    # TODO: is needed here?
    if cost_type == "C" and norm_dc:
        mincost -= sum(2 * len([x for x in tree.leaves()]) - 2 for tree in gene_trees)

    return net, mincost, time.time() - start_time, steps_cnt, moves_cnt


def supernet_search(gene_trees, init_nets, fast_loop=True, shuffle_moves=True,
                    random_state=None, threads=None, verbose=True, relaxed=False,
                    time_consistent=False, cost_type="R", fast_DP=True, norm_dc=True):
    """
    Given input gene trees, for each input initial network search for a locally
    optimal species supernetwork. The network space is traversed using tail
    moves and comparing costs using Dynamic Programming with
    Branch and Bound conflict solver approach.

    Time complexity is linear in number of gene trees and initial networks,
    polynomial in size of taxons set and exponential in number of reticulations.
    Uses multithreading on the level of initial networks to reduce run time.

    Args:
        gene_trees: list of objects of class Tree
        init_nets: list of objects of class Network
        fast_loop: bool for breaking movable-target edges loop when found any
            better candidate
        shuffle_moves: bool for randomizing moves order
        random_state: int for setting seed, works only if shuffle_moves=True
        threads: int or None, how many threads to use in computations, if
            None, sets to a half of total threads
        verbose: bool for printing progress output
        relaxed: bool, whether to relax condition 3b
        time_consistent: bool, whether networks suitable for HGT model
        cost: string, cost to minimise, "C" for deep coalescence cost and "R" for Robinson-Foulds cost
        norm_dc: bool, whether to subtract 2n-2 from DC cost for each tree

    Returns:
        dictionary with lists of outputs for the following keys: net (object of
            class Network), mindc (int), time (float), steps (int), moves (int);
            also contains some summary statistics: best_net (object of class
            Network), best_cost (int), total_time (float), avg_steps (float),
            avg_moves (float)
    """

    # check if gene trees and networks comparable
    labels = {leaf.label for tree in gene_trees for leaf in tree.leaves()}
    for init_net in init_nets:
        missing_labels = labels - set(init_net.get_labels())
        if missing_labels:
            msg = f"There are labels missing in init networks: {missing_labels}"
            raise Exception(msg)

    # prepare output dict
    keys = ['net', 'cost', 'time', 'steps', 'moves']
    output = {key: [None for _ in init_nets] for key in keys}

    # initialize parallelization
    if threads is None:
        threads = int(mp.cpu_count() / 2)
    start_time = time.time()
    pool = mp.Pool(threads)

    # outer parallelized loop
    params = [(gene_trees, net, fast_loop, shuffle_moves, random_state, relaxed, time_consistent, cost_type, fast_DP, norm_dc
               ) for net in init_nets]

    for i, result in enumerate(tqdm(pool.imap(_net_hill_climb, params),
                                    total=len(params), disable=not verbose)):
        for j, key in enumerate(['net', 'cost', 'time', 'steps', 'moves']):
            output[key][i] = result[j]
    pool.close()
    pool.join()

    if output['net']:
        output['net'] = [net for net in output['net'] if net]
        output['best_net'] = output['net'][output['cost'].index(min(output['cost']))]
        output['best_cost'] = min(output['cost'])
        output['total_time'] = time.time() - start_time
        output['avg_steps'] = sum(output['steps']) / len(output['steps'])
        output['avg_moves'] = sum(output['moves']) / len(output['moves'])

    return output
