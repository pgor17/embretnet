from collections import deque

import pytest

from netop import Network
from netsearch import (
    not_comp_msg, are_comparable, contains_subnetwork, is_isomorphic,
    random_treechild_network, get_movable_edges, get_original_edge, 
    get_target_edges, tail_move, tail_moves_gen, supernet_search)
from treeop import Tree, str2tree


def tree1():
    return Network(str2tree('(a,(b,c))'))


def tree2():
    return Network(str2tree('(b,(a,c))'))


def net1():
    return Network(str2tree('((a,#X),((b)#X,c))'))


def net2():
    return Network(str2tree('((((a,(b)#X),((#X,c))#Y),#Y),d)'))


def relaxed():
    return Network(str2tree('((a,((b)#Y)#H),(#H,(c,#Y)))'))


def entangled():
    return Network(str2tree('(((((a,(b)#X),#Y),#X),(x)#Y),d)'))


def cyclic():
    return Network(str2tree('(((a,#X))#Y,((#Y,b))#X)'))


def nonbijective():
    return Network(str2tree('((a,((a)#X)#Y),(#Y,(a,#X)))'))


def nonbinary():
    return Network(str2tree('((#X),(a)#X)'))


def other_taxons():
    return Network(str2tree('((d,#X),((c)#X,b))'))


def tree1_iso():
    return Network(str2tree('((c,b),a)'))


def tree2_iso():
    return Network(str2tree('(b,(c,a))'))


def net1_iso():
    return Network(str2tree('(((b)#S,a),(c,#S))'))


def net2_iso():
    return Network(str2tree('((#T,(((#S,c))#T,((b)#S,a))),d)'))


@pytest.mark.parametrize("net,expected", [
    (tree1(), True), (tree2(), True), (net1(), True), (net2(), True),
    (relaxed(), True), (entangled(), True),
    (cyclic(), False), (nonbijective(), False), (nonbinary(), False)
])
def test_valid_binary(net, expected):
    assert net.valid_binary() == expected

    
@pytest.mark.parametrize("net,expected", [
    (tree1(), True), (tree2(), True), (net1(), True), (net2(), True),
    (relaxed(), True), (entangled(), True),
    (cyclic(), False), (nonbijective(), False), (nonbinary(), False)
])
def test_relaxed(net, expected):
    assert net.type1net() == expected
    
    
@pytest.mark.parametrize("net,expected", [
    (tree1(), True), (tree2(), True), (net1(), True), (net2(), True),
    (relaxed(), False), (entangled(), True),
    (cyclic(), False), (nonbijective(), False), (nonbinary(), False)
])
def test_treechild(net, expected):
    assert net.treechild() == expected
    

@pytest.mark.parametrize("net,expected", [
    (tree1(), True), (tree2(), True), (net1(), True), (net2(), False),
    (relaxed(), True), (entangled(), False)
])
def test_timeconsistent(net, expected):
    assert net.istimeconsistent() == expected


@pytest.mark.parametrize("net1,net2,expected", [
    (tree2(), tree1(), True), (net1(), tree1(), True),
    (net2(), tree1(), False), (net1(), tree2(), True),
    (net2(), tree2(), False), (net2(), net1(), False),
    (tree1(), tree1_iso(), True), (tree2(), tree2_iso(), True),
    (net1(), net1_iso(), True), (net2(), net2_iso(), True),
    (net1(), other_taxons(), False)
])
def test_are_comparable(net1, net2, expected):
    assert are_comparable(net1, net2) == expected
    assert are_comparable(net2, net1) == expected


@pytest.mark.parametrize("net,subnet,expected", [
    (tree2(), tree1(), False), (net1(), tree1(), True),
    (net2(), tree1(), not_comp_msg()), (net1(), tree2(), False),
    (net2(), tree2(), not_comp_msg()), (net2(), net1(), not_comp_msg()),
    (tree1(), tree1_iso(), True), (tree2(), tree2_iso(), True),
    (net1(), net1_iso(), True), (net2(), net2_iso(), True),
    (net1(), other_taxons(), not_comp_msg())
])
def test_contains_subnetwork(net, subnet, expected):
    if type(expected) == str:
        with pytest.raises(Exception) as excinfo:
            contains_subnetwork(net, subnet)
        assert expected == str(excinfo.value)
    else:
        assert contains_subnetwork(net, subnet) == expected


@pytest.mark.parametrize("net1,net2,expected", [
    (tree2(), tree1(), False), (net1(), tree1(), False),
    (net2(), tree1(), not_comp_msg()), (net1(), tree2(), False),
    (net2(), tree2(), not_comp_msg()), (net2(), net1(), not_comp_msg()),
    (tree1(), tree1_iso(), True), (tree2(), tree2_iso(), True),
    (net1(), net1_iso(), True), (net2(), net2_iso(), True),
    (net1(), other_taxons(), not_comp_msg())
])
def test_is_isomorphic(net1, net2, expected):
    if type(expected) == str:
        with pytest.raises(Exception) as excinfo:
            is_isomorphic(net1, net2)
        assert expected == str(excinfo.value)
    else:
        assert is_isomorphic(net1, net2) == expected
        assert is_isomorphic(net2, net1) == expected


@pytest.mark.parametrize("leaves,reticulations", [
    (2, 0), (3, 0), (3, 1), (15, 0), (15, 2), (15, 5),
    ([str(i) for i in range(15)], 5),
    ({'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v'}, 3)
])
def test_random_treechild_network(leaves, reticulations):
    net = random_treechild_network(leaves, reticulations)
    assert net.treechild()
    labels = net.get_labels()
    if hasattr(leaves, '__iter__'):
        assert set(leaves) == set(labels)
    else:
        assert leaves == len(labels)
    assert reticulations == len(net.reticulations)


@pytest.mark.parametrize("net,relaxed,expected_ind", [
    (tree1(), False, [(0, 1), (0, 2), (2, 3), (2, 4)]),
    (net1(), False, [(0, 1), (0, 3), (1, 2), (1, 4), (3, 4), (3, 6)]),
    (net2(), False, [(0, 1), (0, 10), (1, 2), (1, 7), (2, 7), (3, 4), (3, 5), (8, 5)]),
    (net2(), True, [(0, 1), (0, 10), (1, 2), (1, 7), (2, 7), (3, 4), (3, 5), (8, 5), (8, 9)]),
])
def test_get_movable_edges(net, relaxed, expected_ind):
    n = net.nodes
    result = set(get_movable_edges(net, relaxed=relaxed))
    expected = {(n[e[0]], n[e[1]]) for e in expected_ind}
    assert result == expected


@pytest.mark.parametrize("net,movable_edge_ind,expected_ind", [
    (net2(), (0, 1), (10, 10)),
    (net2(), (0, 10), (1, 1)),
    (net2(), (1, 2), (0, 7)),
    (net2(), (1, 7), (0, 2)),
    (net2(), (2, 7), (1, 3)),
    (net2(), (3, 4), (2, 5)),
    (net2(), (3, 5), (2, 4)),
    (net2(), (8, 5), (7, 9)),
    (net2(), (8, 9), (7, 5)),
])
def test_get_original_edge(net, movable_edge_ind, expected_ind):
    n = net.nodes
    m = movable_edge_ind
    result = get_original_edge(net, (n[m[0]], n[m[1]]))
    expected = (n[expected_ind[0]], n[expected_ind[1]])
    assert result == expected


@pytest.mark.parametrize("net,time_consistent,movable_edge_ind,expected_ind", [
    (net2(), False, (0, 1), []),
    (net2(), False, (0, 10), [(1, 2), (1, 7), (2, 7), (7, 8), (8, 9),
                       (8, 5), (5, 6), (2, 3), (3, 5), (3, 4)]),
    (net2(), False, (1, 2), [(0, 0), (0, 10)]),
    (net2(), False, (1, 7), [(0, 0), (0, 10), (3, 4)]),
    (net2(), False, (2, 7), [(3, 4), (0, 0), (0, 10)]),
    (net2(), False, (3, 4), [(2, 7)]),
    (net2(), False, (3, 5), [(0, 10), (0, 0), (0, 1), (1, 2), (7, 8), (8, 9)]),
    (net2(), False, (8, 5), [(0, 10), (0, 0), (0, 1), (1, 2), (2, 3), (3, 4)]),
    (net2(), False, (8, 9), [(0, 10), (0, 0), (0, 1), (1, 2), (2, 3), (3, 4),
                      (3, 5), (5, 6), (1, 7), (2, 7)]),
    # TODO: net2 is not time-consistent so better to make a different example!
    (net2(), True, (0, 1), []),
    (net2(), True, (0, 10), [(1, 7)]),
    (net2(), True, (1, 2), [(0, 0)]),
    (net2(), True, (1, 7), [(0, 10)]),
    (net2(), True, (2, 7), [(0, 10)]),
    (net2(), True, (3, 4), []),
    (net2(), True, (3, 5), []),
    (net2(), True, (8, 5), []),
    (net2(), True, (8, 9), [(1, 7)])
])
def test_get_target_edges(net, time_consistent, movable_edge_ind, expected_ind):
    n = net.nodes
    m = movable_edge_ind
    result = set(get_target_edges(net, (n[m[0]], n[m[1]]), time_consistent))
    expected = {(n[e[0]], n[e[1]]) for e in expected_ind}
    assert result == expected


@pytest.mark.parametrize("net1,movable_edge_ind,target_edge_ind,net2", [
    (tree1(), (0, 1), (2, 4), tree2()),
    (tree1(), (2, 4), (0, 1), tree2()),
    (tree1(), (2, 3), (0, 0), tree2()),
    (net2(), (0, 10), (5, 6),
     Network(str2tree('(((a,((b,d))#X),((#X,c))#Y),#Y)'))),
    (net2(), (1, 2), (0, 0),
     Network(str2tree('(((a,(b)#X),((#X,c))#Y),(#Y,d))'))),
    (net2(), (1, 7), (0, 10),
     Network(str2tree('(((a,(b)#X),((#X,c))#Y),(#Y,d))'))),
    (net2(), (2, 7), (3, 4),
     Network(str2tree('((((a,((#X,c))#Y),(b)#X),#Y),d)'))),
    (net2(), (3, 4), (2, 7),
     Network(str2tree('((((b)#X,(a,((#X,c))#Y)),#Y),d)'))),
    (net2(), (3, 5), (8, 9),
     Network(str2tree('(((a,(((b)#X,(#X,c)))#Y),#Y),d)'))),
    (net2(), (8, 5), (2, 3),
     Network(str2tree('(((((a,(b)#X),#X),(c)#Y),#Y),d)'))),
    (net2(), (0, 10), (7, 8),
     Network(str2tree('(((a,(b)#X),(((#X,c),d))#Y),#Y)'))),
    (net2(), (1, 2), (0, 10),
     Network(str2tree('((((a,(b)#X),((#X,c))#Y),d),#Y)'))),
    (net2(), (3, 5), (0, 0),
     Network(str2tree('((((a,((#X,c))#Y),#Y),d),(b)#X)'))),
    (net2(), (8, 5), (1, 2),
     Network(str2tree('(((((a,(b)#X),(c)#Y),#X),#Y),d)')))
])
def test_tail_move(net1, movable_edge_ind, target_edge_ind, net2):
    n = net1.nodes
    m = movable_edge_ind
    t = target_edge_ind
    tail_move(net1, (n[m[0]], n[m[1]]), (n[t[0]], n[t[1]]))
    assert is_isomorphic(net1, net2)

    any_tree = Tree(str2tree('((a,c),(a,(b,c)))'))
    assert net1.branch_and_bound(any_tree) == net2.branch_and_bound(any_tree)


@pytest.mark.parametrize("net,distance", [
    (tree1(), None), (net1(), None), (net2(), None),
    (random_treechild_network(3, 0), None), (random_treechild_network(3, 1), 1),
    (random_treechild_network(4, 0), None), (random_treechild_network(4, 1), 1),
    (random_treechild_network(4, 2), 2), (random_treechild_network(5, 0), None),
    (random_treechild_network(5, 1), 1), (random_treechild_network(5, 2), 2),
    (random_treechild_network(5, 3), None),
    (random_treechild_network(12, 3), None),
    (random_treechild_network(12, 5), None),
])
def test_tail_moves_gen(net, distance):
    net_copy = Network(str2tree(net.root.netrepr()))
    moves_gen = tail_moves_gen(net, distance, True)

    for move in moves_gen:
        any_moves = True

        # check that moves generated valid tree-child nets on the same taxa
        assert are_comparable(net_copy, net)

        # check that moves generated distinct networks
        assert not is_isomorphic(net_copy, net)

    # check if there were actually any moves
    assert any_moves


@pytest.mark.parametrize("leaves,reticulations,distance", [
    (3, 0, None), (3, 0, None), (3, 0, None),
    (3, 1, 1), (3, 1, 1), (3, 1, 1),
    (4, 0, None), (4, 0, None), (4, 0, None),
    (5, 0, None), (5, 0, None), (5, 0, None),
])
def test_tail_moves_connected(leaves, reticulations, distance):
    net1 = random_treechild_network(leaves, reticulations)
    net2 = random_treechild_network(leaves, reticulations)

    cur_depth = 0
    found_isomorphic = is_isomorphic(net1, net2)
    nets_deque = deque([net1])
    nets_on_cur_depth_left = 1

    # see Lemma 4.3 from "Lost in space? Generalising subtree prune and regraft
    # to spaces of phylogenetic networks" by M. Bordewich et al. (2017)
    depth_limit = 4 * leaves + 8 * reticulations - 2
    # TODO: depth limit in case of limited distance moves?
    # Anyway, this limit is never reached, this test is computationally hard

    while not found_isomorphic and cur_depth <= depth_limit:

        net = nets_deque.popleft()
        nets_on_cur_depth_left -= 1

        for _ in tail_moves_gen(net, distance):
            if is_isomorphic(net, net2):
                found_isomorphic = True
                break
            else:
                net_copy = Network(str2tree(net.root.netrepr()))
                nets_deque.append(net_copy)

        if nets_on_cur_depth_left == 0:
            cur_depth += 1
            nets_on_cur_depth_left = len(nets_deque)

    assert found_isomorphic


@pytest.mark.parametrize("gene_trees,init_nets,expected", [
    ([Tree(str2tree('((a,b),(c,d))')), Tree(str2tree('((a,b),(c,d))'))],
     [Network(str2tree('((a,c),(b,d))')), Network(str2tree('(a,(c,(b,d)))'))],
     {'net': [Network(str2tree('((a,b),(c,d))')),
              Network(str2tree('((a,b),(c,d))'))], 'cost': [12, 12]}),
    ([Tree(str2tree('(a,b)')), Tree(str2tree('(c,d)'))],
     [Network(str2tree('((a,c),(b,d))')), Network(str2tree('(a,(c,(b,d)))'))],
     {'net': [Network(str2tree('((a,b),(c,d))')),
              Network(str2tree('((a,b),(c,d))'))], 'cost': [4, 4]}),
    ([Tree(str2tree('((a,b),c)')), Tree(str2tree('(a,(b,c))'))],
     [net1(), Network(str2tree('((((a)#H1,b),c),#H1)')),
      Network(str2tree('((((c)#H1,b),a),#H1)'))],
     {'net': [net1(), Network(str2tree('((((a)#H1,b),c),#H1)')),
              Network(str2tree('((((c)#H1,b),a),#H1)'))], 'cost': [8, 8, 8]}),
])
def test_supernet_search(gene_trees, init_nets, expected):
    # norm_dc=False
    output = supernet_search(gene_trees, init_nets, cost="C", norm_dc=False, verbose=False)
    nets_i, nets_e = output['net'], expected['net']
    mindcs_i, mindcs_e = output['cost'], expected['cost']
    assert all(is_isomorphic(i, e) for (i, e) in zip(nets_i, nets_e))
    assert all(i == e for (i, e) in zip(mindcs_i, mindcs_e))

    # norm_dc=True
    output = supernet_search(gene_trees, init_nets, cost="C", norm_dc=True, verbose=False)
    nets_i, nets_e = output['net'], expected['net']
    mindcs_i, mindcs_e = output['cost'], [0 for _ in output['cost']]
    assert all(is_isomorphic(i, e) for (i, e) in zip(nets_i, nets_e))
    assert all(i == e for (i, e) in zip(mindcs_i, mindcs_e))
