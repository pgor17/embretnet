from timeit import default_timer as timer

import pandas as pd
from tqdm import tqdm


from netop import Network
from treeop import Tree, str2tree, compcostsmp

STAT_NAMES = ("dp_called", "branching_count", "max_depth", "best_depth", "time_in_seconds")
STAT_NAMES_FAST_ALGORITHM = (f"{stat_name}_fast_algo" for stat_name in STAT_NAMES)


def save_bnb_stats(network_str, tree_str, use_fast_algo):
    network, tree = Network(str2tree(network_str)), Tree(str2tree(tree_str))
    best_cost_bnb, stats = network.branch_and_bound(tree, return_stats=True, fast_DP=use_fast_algo)
    stats = [stats[name] for name in STAT_NAMES]
    return stats, best_cost_bnb


def save_naive_time(network_str, tree_str):
    network, tree = Network(str2tree(network_str)), Tree(str2tree(tree_str))
    min_cost = float("inf")
    start_time = timer()
    for t in network.displayedtrees():
        min_cost = min(min_cost, compcostsmp(tree, Tree(str2tree(t)), "R"))
    end_time = timer()
    naive_time = end_time - start_time
    return naive_time, min_cost


def save_naive_time_C(network_str, tree_str):
    network, tree = Network(str2tree(network_str)), Tree(str2tree(tree_str))
    min_cost = float("inf")
    start_time = timer()
    for t in network.displayedtrees():
        min_cost = min(min_cost, tree.rfcost2(Tree(str2tree(t))))
    end_time = timer()
    naive_time = end_time - start_time
    return naive_time, min_cost


def save_stats(network_str, tree_str):
    network_str, tree_str = network_str.strip(";"), tree_str.strip(";")
    stats_slow_algo, cost_slow_algo = save_bnb_stats(network_str, tree_str, use_fast_algo=False)
    stats_fast_algo, cost_fast_algo = save_bnb_stats(network_str, tree_str, use_fast_algo=True)
    time_naive, cost_naive = save_naive_time(network_str, tree_str)
    time_naive_C, cost_naive_C = save_naive_time_C(network_str, tree_str)
    full_stats = stats_slow_algo + [cost_slow_algo] + stats_fast_algo + [cost_fast_algo, time_naive, cost_naive, time_naive_C, cost_naive_C]
    return full_stats


if __name__ == "__main__":
    df = pd.read_csv("../data/preprocessed_simulation_data.csv")
    full_labels = [f"{stat}_slow_algo" for stat in STAT_NAMES] + ["cost_slow_algo"] + [f"{stat}_fast_algo" for stat in
                                                                                       STAT_NAMES] + ["cost_fast_algo",
                                                                                                      "time_naive",
                                                                                                      "cost_naive",
                                                                                                      "time_naive_C",
                                                                                                      "cost_naive_C"]
    tqdm.pandas()
    output_df = df.progress_apply(
        lambda row: pd.Series(save_stats(row['network'], row['gene_tree_rooted']), index=full_labels), axis=1)
    result_df = pd.concat([df, output_df], axis=1)
    result_df.to_csv("results_amor_priority_and_parcut.csv")
