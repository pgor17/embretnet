#!/usr/bin/python3

import getopt
import itertools
import sys

from netop import Network, randdagstr
from netsearch import supernet_search, get_init_nets
from treeop import Tree, str2tree, ftread, compcostsmp, quasiconsensusgen, gendotfile
from netroot import phiSupports, phiSupports2str

VERBOSE = ""
PRINTS_TREE_WITH_SCORES = 0
INF = 10 ** 9
DEFAULT_COST_TYPE = 'C'
DEFAULT_COST_NAME = 'DC (non-classic)'

def usage():
    print("""Usage:
 -g FILE/TREE(S) - input gene tree(s); multiple trees can be defined in a ;-separated string;
 -s FILE/TREE(S) - input species tree(s)
 -n FILE/TREE(S) - reticulation network(s) in rich newick format:   
  ((a, ((b, (c, (d)Y#H1)), (((Y#H1, e)h, f))X#H2)), ((X#H2, g)d, 8)b)r;
Cardona et al., 2008 BMC Bioinf.; comments are allowed in [...]

 -c COST - cost function C=DC(non-classic, default), D=DUP, L=LOSS, U=DL, R=RF, c=DC(classic); e.g. -c U 

 -d filename - gen dot file(s); see -pi
 
 -x - if specified, use heuristics in branch-and-bound
 --limit INT - how many branchings can we use before halting in branch-and-bound, this might result in incorrect result

 -p STR - print & evaluate: 
     s - print species tree
     n - print network
     u - print networks using decomposion and sorted represenation (only for tree-child networks)
     d - print all trees displayed by the network (see also -i)
     l - print all trees displayed by the network with branch lenghts (see also -i)
     e - DP for gene tree-network embedding (only C, R, D costs); see -pF
     F - do not use fast DP with RF 
     D - info on nodes of each network (debug)
     c - compare gene and species tree 
     a - compare gene and all trees displayed by the network
     b - computing ODT cost by branch-and-bound algorithm; may call DP multiple times 
         only with C, R, D; see also -pF for fast DP with RF     
     A - print Phi supports for each network; see also suboption r
     Ar - as above but supports are computed only for the root of the input networks (gene trees ignored if present)
     t - for every network print True if the network is tree-child otherwise print False
     i - skip node ids in labels of nodes in dot generator (-d)

 -i NUM - print display tree no NUM (NUM in 0..2^reticulations-1)

 -q NUM - gen NUM quasi-consensus trees from gene trees

 -C "label1,label2,..." - remove leaves having given labels from all networks
 -P "label1,label2,..." - preserve leaves having given labels in all networks
 -R - remove redundant reticulations (both parents of reticulation are the same or grandparent is the second parent)

 -S - run super-network heuristic; 
 -k NUM - maximal number of reticulations in a super-network; default is 5

 -D - compute RF-net distance between each pair of networks

Compare gene tree vs. species tree (DUP cost)
embnet.py  -g "(((c,b),a),(e,d))" -s"(((b,a),c),(e,d))" -cD -pc

Compare gene tree vs. species tree (DC cost non-classic)
embnet.py  -g "(((c,b),a),(e,d))" -s"(((b,a),c),(e,d))" -pc

Print trees displayed by the network:
embnet.py -n "((a, ((b)#Y)#H ), (#H, (c,#Y)))" -pd
         
DP with C, R, D only; see also -pF for fast DP with RF)::

DP algorithmEmbedding gene trees into networks: DP with DC (lower bound computation), DP with C, R, D only; see also -pF for fast DP with RF)::
embnet.py -n "((a, (b)#H ), (#H, c) )" -g  "((a,b),(b,c))" -pe
embnet.py -n "((a, (b)#H ), (#H, c) )" -g  "(b,c)" -pe
embnet.py -n "((((f, e), b))#H1, (#H1, (((d, c))#H2, (#H2, a))))" -g "((c, ((e, a), b)), d)" -pe

Embedding gene trees into networks with branch-and-bound (DP with C, R, D only; see also -pF for fast DP with RF):
embnet.py -n "((a, (b)#H ), (#H, c) )" -g  "((a,b),(b,c))" -pb
embnet.py -n "((a, (b)#H ), (#H, c) )" -g  "(a,(b,c))" -pb -cR 
embnet.py -n "((((f, e), b))#H1, (#H1, (((d, c))#H2, (#H2, a))))" -g "((c, ((e, a), b)), d)" -pb -cD

Generate and view dot file (-d)
embnet.py -n "((a, ((b)#Y)#H ), (#H, (c,#Y))) [Non tree-child N1]" -g "(a, b)"  -pe -d n1
dot -Tpdf n1.dot -o n1.pdf # graphviz package required

Compute by DP, generate and view dot (file with delta annotations for nodes 0, 1 and 2 from the gene tree.
embnet.py -n "((a, ((b)#Y)#H ), (#H, (c,#Y))) [Non tree-child N1]" -g "(a, b)"  -pe -d "n1delta:0:1:2"
dot -Tpdf n1delta.dot -o n1delta.pdf

Naive cost computation by comparing a given gene tree with all displayed trees (DUP cost)
embnet.py -g  "((a,b),c)"  -n "((a, (b)#H ), (#H, c) )" -pa -cD

Super-network inference using some predefined set of initial networks
embnet.py -S -g"((a,b),c); ((a,a),(b,c))" -n"((a, (b)#H ), (#H, c) ); (a,(b,c))"

Naive DC-cost computation by comparing a given gene tree with all displayed trees 
embnet.py -g  "((a,b),(b,c))"  -n "((a, (b)#H ), (#H, c) )" -pa

To generate random tree-child networks/trees in -n, -g or -s use rand:NUM1:NUM2 where NUM1 is the number of leaves 
and NUM2 is the number of reticulation nodes. Leaf labels are a,b,c,...,z,a1,b1,... etc, and reticulation labels are A,B,...,Z,A1,A2,...
embnet.py -n rand:5:3 -g rand:5:0
(((#C,(a)#A),(((d,b))#B)#C),(c,(#A,(#B,e))))
((a,e),((b,c),d))

Dot-drawing random emb0eddings:
embnet.py -nrand:10:5 -grand:10:0 -pe -dn && dot -Tpdf n.dot -o n.pdf 

Dot-drawing random network:
embnet.py -nrand:10:5 -dn && dot -Tpdf n.dot -o n.pdf 
embnet.py -nrand:100:5 -dn && dot -Tpdf n.dot -o n.pdf && evince n.pdf

RF-net comparing:
embnet.py -n "(#A,(d,((a,c),(b,(e)#A)))); (#A,(d,((a,c),(b,(e)#A))))" -D
embnet.py -n "((c,b),(#A,((d,a),(e)#A))); (#A,(d,((a,c),(b,(e)#A)))); (#A,(d,((a,c),(b,(e)#A))))" -D

Displayed trees with branch lengths:
embnet.py -n"((c:2)#A:4,(#A:1,(b:2,a:5):1):2):2" -pnd
embnet.py -n"((c:2)#A:4,(#A:1,(b,a:5)):2):2" -pnd

Contract:
embnet.py -n"((((b)#A,c))#B,(#B,(#A,(a,d))))" -C "a,d" -pn

Remove redundant reticulations:
embnet.py -n '((((b)#A,((c)#C,#C)))#B,(#A,#B))' -R -pn

Print sorted representation (unique for TC-networks)
embnet.py -n '((((b)#A,((c)#C,#C)))#B,(#A,#B))' -R -pn

Computing Phi supports (used in gene tree rooting via networks)
embnet.py -n '((a,b),(c,d))' -g "(c,d)" -pA 
embnet.py -n '((a,b),(c,d))' -g "(c,d)" -pAr 
embnet.py -n '((((b)#A,((c)#C,#C)))#B,(#A,#B))' -pA 

Test whether the network is tree-child.
embnet.py -n '((((b)#A,((c)#C,#C)))#B,(#A,#B))' -pt

""")





def main():
    # parse command line options
    try:
        opts, _ = getopt.getopt(sys.argv[1:], "s:g:n:p:c:C:G:RNtSd:r:x:hk:Sq:D:i:PK:", ["help", "limit=", "output="])
    except getopt.GetoptError as err:
        print(str(err))
        usage()
        sys.exit(2)
    if len(sys.argv) == 1:
        usage()
        sys.exit(1)

    gtrees = []
    strees = []
    networks = []
    global VERBOSE, PRINTS_TREE_WITH_SCORES, delnodes

    delnodes = ""
    PRINTS_TREE_WITH_SCORES = 0
    printevalopt = []

    cost_name = DEFAULT_COST_NAME
    cost_type = DEFAULT_COST_TYPE
    dotfile = ""
    dotspec = ""
    use_branch_and_bound_already_set_nodes = False
    branch_and_bound_branch_limit = INF
    reticulationscnt = 5
    optsupernetwork = False
    quasiconsensus = 0
    rfnet = 0
    displaytreeid = None
    removeleaves = None    
    preserveleaves = None    
    removeredunantreticulations = False
    nets_number = 1


    for o, a in opts:
        # rand trees/networks preprocessing
        if o[0] == '-' and o[1] in "ngs":
            t = a.split(":")
            if len(t) == 3 and t[0] == 'rand':
                a = randdagstr(int(t[1]), int(t[2]), 0)  # now tree child by default
            if len(t) == 3 and t[0] == 'nt1':
                a = randdagstr(int(t[1]), int(t[2]), 1)
            if len(t) == 3 and t[0] == 'gen':
                a = randdagstr(int(t[1]), int(t[2]), 2)
            if not a:
                print("No network/tree is generated...")
                continue
            # print(a)

        if o == "-n":
            networks.extend(ftread(a, Network))
        elif o == "-i":
            displaytreeid = int(a)
        elif o == "-g":
            gtrees.extend(ftread(a, Tree))
        elif o == "-s":
            strees.extend(ftread(a, Tree))
        elif o == "-S":
            optsupernetwork = 1
        elif o == "-k":
            reticulationscnt = int(a)
        elif o == "-K":
            nets_number = int(a)
        elif o == "-P":
            preserveleaves = a.split(",")
        elif o == "-C":
            removeleaves = a.split(",")
        elif o == "-R":
            removeredunantreticulations = True
        elif o == "-q":
            quasiconsensus = int(a)
        elif o == "-p":
            printevalopt = str(a)
        elif o == "-D":
            rfnet = 1
        elif o == "-d":
            a = str(a).split(":")  # first is label : next are node ids from gene tree
            dotfile, dotspec = a[0], map(int, a[1:])

        elif o == "-c":
            cost_type = a
            if a == 'D':
                cost_name = "DUP"
            elif a == 'L':
                cost_name = "LOSS"
            elif a == 'U':
                cost_name = "DL"
            elif a == 'C':
                cost_name = "DC (non-classic)"
            elif a == 'c':
                cost_name = "DC (classic)"
            elif a == 'R':
                cost_name = "RF"
            else:
                print("Unknown cost", a)
        elif o in ("-h", "--help"):
            usage()
            sys.exit()
        elif o == '-x':
            use_branch_and_bound_already_set_nodes = True
        elif o == '--limit':
            branch_and_bound_branch_limit = int(a)
        else:
            assert False, "unhandled option %s %s " % (o, a)

    """((a)#X,(((#X,b))#Y,((#Y,c),d)))"""


    if removeleaves:                                
        networks = [ Network(str2tree(n.contractbyleaves(removeleaves))) for n in networks ]                
    if preserveleaves:                                
        networks = [ Network(str2tree(n.contractbyleaves(preserveleaves,True))) for n in networks ]                

    if removeredunantreticulations:

        n2 = []
        for n in networks:        
            while (rt:=n.findredundantreticulation()) is not None:                
                n = Network(str2tree(n.contractret(rt)))
            n2.append(n)
        networks = n2

    if 't' in printevalopt:
        for n in networks:
            print(n.treechild()==True)                
        sys.exit(0)
                            

    if 'g' in printevalopt:
        for g in gtrees:
            print(g)

    if 's' in printevalopt:
        for s in strees:
            print(s)

    if 'n' in printevalopt:
        for n in networks:
            print(n)

    fast_DP = 'F' not in printevalopt

    if 'u' in printevalopt:
        for n in networks:
            print(n.root.netreprsorted())

    if 'D' in printevalopt:
        for n in networks:
            n.ptab()

    # Cost computation between gene trees and species trees
    if 'c' in printevalopt:
        for g, s in itertools.product(gtrees, strees):
            print(g, s, compcostsmp(g, s, cost_type), cost_name)            

    if 'd' in printevalopt:
        for n in networks:
            print("Network:", n)
            for t in n.displayedtrees():
                print(t)

    if displaytreeid is not None:
        for n in networks:
            print("Network:", n)            
            print(n.displayedtreebyid(displaytreeid))                
    
    tcnetworks = networks # ???
    nontcnetworks = len(networks) - len(tcnetworks)

    if nontcnetworks and ('e' in printevalopt or 'b' in printevalopt):  # TODO add verbose options
        print(f"{nontcnetworks} non tree-child network(s) ignored", file=sys.stderr)

    # Lower bound by DP
    if 'e' in printevalopt:
        for g, n in itertools.product(gtrees, tcnetworks):

            if cost_type == 'C':
                cost, _, opt_map = n.retmindc(g, additional_return=True, dotspec=dotspec)
            elif cost_type == 'D':
                cost, _, opt_map = n.retmindup(g, additional_return=True)
            elif cost_type == 'R' and fast_DP:
                _, cost, opt_map = n.ret_min_rf_fast(g)                
            elif cost_type == 'R' and not fast_DP:
                _, cost, opt_map = n.ret_min_rf(g)
            else:
                cost = None

            if cost is None:
                print(f"DP with {cost_name} is not implemented yet")
            else:
                print(f"{cost}")

    # ODT by B&B algorithm
    if 'b' in printevalopt:
        for g, n in itertools.product(gtrees, tcnetworks):
            print(n.branch_and_bound(g, use_branch_and_bound_already_set_nodes, 
                branch_and_bound_branch_limit, cost_type=cost_type, fast_DP=fast_DP))

    if 'a' in printevalopt:
        for g, n in itertools.product(gtrees, networks):
            print("Network:", n)
            for t in n.displayedtrees():
                print(g, t, compcostsmp(g, Tree(str2tree(t)), cost_type), cost_type)

    # Run super-network search
    if optsupernetwork:
        initnets = tcnetworks

        # TODO: expand
        if not initnets:
            initnets = get_init_nets(gtrees, reticulationscnt, nets_number,
                      init_method='quasi-cons', 
                      relaxed=False, 
                      time_consistent=False)

        out = supernet_search(gtrees, initnets, cost_type=cost_type, fast_DP=fast_DP)
        if out['net']:
            print(f"Best cost: {out['best_cost']}")
            print(f"Best net: {out['best_net']}")
        else:
            print("No initial networks found (use '-n', )")

    # gen dot file
    if dotfile and networks:
        p = list(itertools.product(gtrees if gtrees else [None], networks))
        if len(p) > 1:
            for i, (g, n) in enumerate(p):
                gendotfile([g, n], "%s%d.dot" % (dotfile, i), addnodeidcomments='i' not in printevalopt)
        else:
            gendotfile([p[0][0], p[0][1]], "%s.dot" % dotfile, addnodeidcomments='i' not in printevalopt)

    if quasiconsensus:
        for s in quasiconsensusgen(gtrees, quasiconsensus):
            print(s)

    if rfnet:
        for n1, n2 in itertools.combinations(tcnetworks, 2):
            print(n1.robinsonfouldsnet(n2), n1, n2)

    if 'A' in printevalopt:         
        for network in tcnetworks: 
            for net, dc, lev in phiSupports(network, gtrees, 'Ar' in printevalopt):
                print(net, phiSupports2str(dc), lev)                                

    if 'S' in printevalopt:         
        
        for network in tcnetworks: 
            for gtree in gtrees: 
            
                u = network.unfold()            
                t = Tree(u)        
                
                res = t.scenarioenum(gtree, network, genalldots=False)

                exit(res)


            
        
    return 0


if __name__ == "__main__":
    main()
