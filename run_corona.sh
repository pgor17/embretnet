#!/bin/bash
gtree_nr_values=(5 10)
fastloop_values=("fastloop" "no-fastloop")
rspec_values=(20 0)
rcons_values=(0 20)

for gtree_nr in "${gtree_nr_values[@]}"; do
    for fastloop in "${fastloop_values[@]}"; do
        for rspec in "${rspec_values[@]}"; do
            for rcons in "${rcons_values[@]}"; do
                output_name="gtree_nr${gtree_nr}_fastloop${fastloop}_rspec${rspec}_rcons${rcons}"

                python3 test_hc_corona.py --gtrees "$gtree_nr" --"$fastloop" --rspec "$rspec" --rcons "$rcons" -f "$output_name"
            done
        done
    done
done
