import argparse
import numpy as np
import pandas as pd
import plotly as py
import plotly.express as px
import matplotlib.pyplot as plt

from tqdm import tqdm
from random import seed, sample
from itertools import product
from treeop import Tree, str2tree
from netop import Network
from netsearch import random_treechild_network, get_init_nets, supernet_search


def main():
    
    example_txt = """example:   
    python3 test_hc_random.py -r2 --nets 5 --nmin 4 --nmax 6 --kmax 2
    """
    
    desc = """Run netsearch quality tests on random networks.
    Uses network as model and its displayed trees as input gene trees."""
    
    parser = argparse.ArgumentParser(description=desc, epilog=example_txt,
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-f", help="Target filename to save .html interactive report", type=str, default="hc_random_table.html")
    parser.add_argument("-r", help="Number of initial networks used in each run", type=int, default=15)
    parser.add_argument("--nets", help="Number of nets tested for each combination of parameters", type=int, default=20)
    parser.add_argument("--nmin", help="Minimum number of leaves", type=int, default=5)
    parser.add_argument("--nmax", help="Maximum number of leaves", type=int, default=10)
    parser.add_argument("--kmin", help="Minimum number of reticulations", type=int, default=1)
    parser.add_argument("--kmax", help="Maximum number of reticulations", type=int, default=3)
#     parser.add_argument("--disp_frac", help="Fraction of displayed trees used for reconstruction", type=float, default=1.)
    parser.add_argument("--verbose", help="Print progress bar (1-print, 0-silent)", type=int, default=1)
    parser.add_argument("--seed", help="Seed for all random operations", type=int, default=1)
    args = parser.parse_args()

    if args.nmin < 3:
        print(f"Argument 'nmin' too low ({args.nmin}), setting to 3")
        args.nmin = 3
    if args.nmin > args.nmax:
        print(f"Error: 'nmin' ({args.nmin}) can't be greater than 'nmax' ({args.nmax})")
        exit(1)
    if args.kmin < 1:
        print(f"Argument 'kmin' too low ({args.kmin}), setting to 1")
        args.kmin = 1
    if args.kmin > args.kmax:
        print(f"Error: 'kmin' ({args.kmin}) can't be greater than 'kmax' ({args.kmax})")
        exit(1)
    if args.nmin < args.kmax + 2:
        print(f"Error: 'nmin' ({args.nmin}) can't be lower than 'kmax'+2 ({args.kmax+2})")
        exit(1)
    if args.nets < 1:
        print(f"Argument 'nets' too low ({args.nets}), setting to 1")
        args.nets = 1
    if args.r < 1:
        print(f"Argument 'r' too low ({args.r}), setting to 1")
        args.r = 1
#     if args.disp_frac < 0:
#         print(f"Argument 'disp_frac' too low ({args.disp_frac}), setting to 0")
#         args.disp_frac = 0.
#     if args.disp_frac > 1:
#         print(f"Argument 'disp_frac' too high ({args.disp_frac}), setting to 1")
#         args.disp_frac = 1.
    
    n_range = range(args.nmin, args.nmax+1)
    k_range = range(args.kmin, args.kmax+1)
    columns = ['Leaves', 'Reticulations', 'Generalized RF distance', 'Divide', 'Trees fraction']
    stats = []
    
    for n, k in tqdm(product(n_range, k_range), total=len(n_range)*len(k_range),
                     disable=not args.verbose):
#         disp_number = max(int(2**k * args.disp_frac), 1)
        disp_number = max(int(2**k * 0.5), 1)
        seed(args.seed)
        nets = [random_treechild_network(n, k) for _ in range(args.nets)]
        for net in nets:
            disp_trees_all = [Tree(str2tree(t)) for t in net.displayedtrees()]
            for perc, disp_trees in zip(['100%', '50%'], [disp_trees_all, sample(disp_trees_all, disp_number)]):
                init_nets = get_init_nets(disp_trees, k, args.r)
                best_net = supernet_search(disp_trees, init_nets, verbose=False,
                                          random_state=args.seed)['best_net']
                best_net = Network(str2tree(best_net.root.netrepr()))
                rfoulds = best_net.robinsonfouldsnet(net)
                stats.append([f'n={n}', f'k={k}', rfoulds/40, '40', perc])
                stats.append([f'n={n}', f'k={k}', rfoulds/(2**(k+1)*(n-2)), '2(n-2)2^k', perc])
     
    stats = pd.DataFrame(stats, columns=columns)
    title = f"Generalized Robinson-Foulds distances between the network and "
    title += f"its reconstruction from displayed trees, " 
    title += f"sample-size={args.nets}, init-nets={args.r}"""
    
    fig = px.box(stats, x='Leaves', y='Generalized RF distance', color='Reticulations',
                 facet_row='Divide', facet_col='Trees fraction', title=title)
    fig.update_layout(legend=dict(x=0.02, y=0.95, bgcolor="White", bordercolor="Black", borderwidth=2))
    py.offline.plot(fig, filename=args.f)
#     fig.show()
  
    
if __name__ == "__main__":
    main()
